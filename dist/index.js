"use strict";

var _args = require("@boost/args");

var _Log = require("./util/Log");

var _Config = require("./Config");

var _Context = require("./Context");

var _fslib = require("@yarnpkg/fslib");

var _vfile = require("vfile");

var _toVfile = _interopRequireDefault(require("to-vfile"));

var _vfileGlob = _interopRequireDefault(require("vfile-glob"));

var _interpolateUrl = _interopRequireDefault(require("interpolate-url"));

var _pQueue = _interopRequireDefault(require("p-queue"));

var _Common = require("./tasks/Common");

var _Script = require("./tasks/Script");

var _Remark = require("./tasks/Remark");

var _Pug = require("./tasks/Pug");

var _Yaml = require("./tasks/Yaml");

var _Assets = require("./tasks/Assets");

var _Flow = require("./util/Flow");

var _UTI = _interopRequireDefault(require("./util/UTI"));

var _os = _interopRequireDefault(require("os"));

var _pug = _interopRequireDefault(require("pug"));

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(async () => {
  const argv = process.argv.slice(2);
  const args = (0, _args.parse)(argv, {
    commands: ['build'],
    options: {
      baseDir: {
        description: 'Base directory of the site to build',
        type: 'string',
        short: 'C',
        default: await (0, _Config.findBaseDir)(_fslib.xfs),

        validate(value) {
          return _fslib.xfs.existsSync(value);
        }

      }
    }
  });
  const log = (0, _Log.createLogger)();
  log.level = 'trace';
  log.info("Umami - a savoury static site generator");
  log.info("Using base directory: %s", args.options.baseDir);
  const config = await (0, _Config.loadConfiguration)(args.options.baseDir, _fslib.xfs);
  const workQueue = new _pQueue.default({
    concurrency: _os.default.cpus().length + 1
  });
  const ctx = new _Context.UmamiContext(log, config, workQueue);
  const collectionEntryFlows = {
    'net.daringfireball.markdown': new _Flow.Flow(workQueue).add(_Common.loadContent).add(_Remark.transformMarkdown).add(_Remark.postFrontmatter).add(_Assets.processAssets),
    'org.yaml': new _Flow.Flow(workQueue).add(_Common.loadContent).add(_Common.fnvHash).add(_Yaml.parseYaml).add(_Assets.processAssets)
  };

  for (const i in config.collections) {
    const collection = config.collections[i];
    (0, _vfileGlob.default)(collection.glob).subscribe(vfile => {
      vfile.data.collection = collection.name;

      const collectionEntryFlow = _lodash.default.find(collectionEntryFlows, (flow, type) => {
        return _UTI.default.fileNameConformsTo(vfile.path, type);
      });

      if (collectionEntryFlow) collectionEntryFlow.execute(ctx, vfile);
    });
  }

  await workQueue.onIdle();
  const scriptFlow = new _Flow.Flow(workQueue).add(_Common.loadContent).add(_Script.parseBabel).add(_Script.extractImports).add(_Script.importTree).add(_Script.transpile).add(async (c, v) => {
    v.path = _fslib.ppath.join(c.scriptsDir, v.data.moduleName);
    v.extname = '.js';
    return v;
  }).add(_Common.saveContent).add(_Common.brotliCompressTransparent).add(_Common.gzipCompressTransparent);

  for (const i in config.scripts) {
    const vfile = (0, _toVfile.default)(_fslib.ppath.join(ctx.baseDir, config.scripts[i].src));
    vfile.data.moduleName = vfile.stem;
    await scriptFlow.execute(ctx, vfile);
  }

  let importMap = {
    imports: {}
  };
  const allScripts = ctx.scriptDependencies.overallOrder();

  for (const i in allScripts) {
    const script = ctx.scriptDependencies.getNodeData(allScripts[i]);
    importMap.imports[script.data.moduleName] = _fslib.ppath.relative(ctx.outputDir, script.path);
  }

  await ctx.fs.writeFilePromise(_fslib.ppath.join(ctx.scriptsDir, 'importmap.json'), JSON.stringify(importMap));

  for (const i in config.collections) {
    const collection = config.collections[i];

    for (const j in collection.views) {
      const view = collection.views[j];
      ctx.log.trace("Processing view %s for collection %s", view.src, collection.name);
      const entries = ctx.getCollection(collection.name).value();

      const template = _pug.default.compileFile(_fslib.ppath.join(ctx.baseDir, view.src));

      const collectionViewFlow = new _Flow.Flow(workQueue).add(_Yaml.resolveXrefs).add((0, _Pug.renderPug)(template)).add(_Common.saveContent).add(_Common.brotliCompressTransparent).add(_Common.gzipCompressTransparent);

      for (const k in entries) {
        const entry = _lodash.default.cloneDeep(entries[k]);

        entry.data.permalink = (0, _interpolateUrl.default)(view.uri, entry.data);
        if (view.canonical) entries[k].data.permalink = entry.data.permalink;
        entry.path = _fslib.ppath.join(ctx.outputDir, _lodash.default.trimStart(entry.data.permalink, '/'));
        await collectionViewFlow.execute(ctx, entry);
      }
    }
  }

  for (const i in config.pages) {
    const pageRenderFlow = new _Flow.Flow(workQueue);
    const page = config.pages[i];
    let vfile = !!page.contents ? (0, _toVfile.default)(_fslib.ppath.join(ctx.baseDir, page.contents)) : new _vfile.VFile();

    const template = _pug.default.compileFile(_fslib.ppath.join(ctx.baseDir, page.template));

    vfile.data.permalink = page.uri;

    if (!!page.contents) {
      pageRenderFlow.add(_Common.loadContent).add(_Remark.transformMarkdown);
    }

    pageRenderFlow.add(_Remark.postFrontmatter).add(_Assets.processAssets).add(async (c, v) => {
      v.path = _fslib.ppath.join(c.outputDir, _lodash.default.trimStart(v.data.permalink, '/'));
      return v;
    }).add((0, _Pug.renderPug)(template)).add(_Common.saveContent).add(_Common.brotliCompressTransparent).add(_Common.gzipCompressTransparent);
    await pageRenderFlow.execute(ctx, vfile);
  }

  await workQueue.onIdle();
})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC50cyJdLCJuYW1lcyI6WyJhcmd2IiwicHJvY2VzcyIsInNsaWNlIiwiYXJncyIsImNvbW1hbmRzIiwib3B0aW9ucyIsImJhc2VEaXIiLCJkZXNjcmlwdGlvbiIsInR5cGUiLCJzaG9ydCIsImRlZmF1bHQiLCJ4ZnMiLCJ2YWxpZGF0ZSIsInZhbHVlIiwiZXhpc3RzU3luYyIsImxvZyIsImxldmVsIiwiaW5mbyIsImNvbmZpZyIsIndvcmtRdWV1ZSIsInBRdWV1ZSIsImNvbmN1cnJlbmN5Iiwib3MiLCJjcHVzIiwibGVuZ3RoIiwiY3R4IiwiVW1hbWlDb250ZXh0IiwiY29sbGVjdGlvbkVudHJ5Rmxvd3MiLCJGbG93IiwiYWRkIiwibG9hZENvbnRlbnQiLCJ0cmFuc2Zvcm1NYXJrZG93biIsInBvc3RGcm9udG1hdHRlciIsInByb2Nlc3NBc3NldHMiLCJmbnZIYXNoIiwicGFyc2VZYW1sIiwiaSIsImNvbGxlY3Rpb25zIiwiY29sbGVjdGlvbiIsImdsb2IiLCJzdWJzY3JpYmUiLCJ2ZmlsZSIsImRhdGEiLCJuYW1lIiwiY29sbGVjdGlvbkVudHJ5RmxvdyIsIl8iLCJmaW5kIiwiZmxvdyIsIlVUSSIsImZpbGVOYW1lQ29uZm9ybXNUbyIsInBhdGgiLCJleGVjdXRlIiwib25JZGxlIiwic2NyaXB0RmxvdyIsInBhcnNlQmFiZWwiLCJleHRyYWN0SW1wb3J0cyIsImltcG9ydFRyZWUiLCJ0cmFuc3BpbGUiLCJjIiwidiIsInBwYXRoIiwiam9pbiIsInNjcmlwdHNEaXIiLCJtb2R1bGVOYW1lIiwiZXh0bmFtZSIsInNhdmVDb250ZW50IiwiYnJvdGxpQ29tcHJlc3NUcmFuc3BhcmVudCIsImd6aXBDb21wcmVzc1RyYW5zcGFyZW50Iiwic2NyaXB0cyIsInNyYyIsInN0ZW0iLCJpbXBvcnRNYXAiLCJpbXBvcnRzIiwiYWxsU2NyaXB0cyIsInNjcmlwdERlcGVuZGVuY2llcyIsIm92ZXJhbGxPcmRlciIsInNjcmlwdCIsImdldE5vZGVEYXRhIiwicmVsYXRpdmUiLCJvdXRwdXREaXIiLCJmcyIsIndyaXRlRmlsZVByb21pc2UiLCJKU09OIiwic3RyaW5naWZ5IiwiaiIsInZpZXdzIiwidmlldyIsInRyYWNlIiwiZW50cmllcyIsImdldENvbGxlY3Rpb24iLCJ0ZW1wbGF0ZSIsInB1ZyIsImNvbXBpbGVGaWxlIiwiY29sbGVjdGlvblZpZXdGbG93IiwicmVzb2x2ZVhyZWZzIiwiayIsImVudHJ5IiwiY2xvbmVEZWVwIiwicGVybWFsaW5rIiwidXJpIiwiY2Fub25pY2FsIiwidHJpbVN0YXJ0IiwicGFnZXMiLCJwYWdlUmVuZGVyRmxvdyIsInBhZ2UiLCJjb250ZW50cyIsIlZGaWxlIl0sIm1hcHBpbmdzIjoiOztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUdBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOzs7O0FBRUEsQ0FBQyxZQUFZO0FBRWIsUUFBTUEsSUFBSSxHQUFHQyxPQUFPLENBQUNELElBQVIsQ0FBYUUsS0FBYixDQUFtQixDQUFuQixDQUFiO0FBRUEsUUFBTUMsSUFBSSxHQUFHLGlCQUFVSCxJQUFWLEVBQWdCO0FBQ3pCSSxJQUFBQSxRQUFRLEVBQUUsQ0FBQyxPQUFELENBRGU7QUFFekJDLElBQUFBLE9BQU8sRUFBRTtBQUNMQyxNQUFBQSxPQUFPLEVBQUU7QUFDTEMsUUFBQUEsV0FBVyxFQUFFLHFDQURSO0FBRUxDLFFBQUFBLElBQUksRUFBRSxRQUZEO0FBR0xDLFFBQUFBLEtBQUssRUFBRSxHQUhGO0FBSUxDLFFBQUFBLE9BQU8sRUFBRSxNQUFNLHlCQUFZQyxVQUFaLENBSlY7O0FBS0xDLFFBQUFBLFFBQVEsQ0FBRUMsS0FBRixFQUFTO0FBQUUsaUJBQU9GLFdBQUlHLFVBQUosQ0FBZUQsS0FBZixDQUFQO0FBQThCOztBQUw1QztBQURKO0FBRmdCLEdBQWhCLENBQWI7QUFjQSxRQUFNRSxHQUFHLEdBQUcsd0JBQVo7QUFDQUEsRUFBQUEsR0FBRyxDQUFDQyxLQUFKLEdBQVksT0FBWjtBQUVBRCxFQUFBQSxHQUFHLENBQUNFLElBQUosQ0FBUyx5Q0FBVDtBQUNBRixFQUFBQSxHQUFHLENBQUNFLElBQUosQ0FBUywwQkFBVCxFQUFxQ2QsSUFBSSxDQUFDRSxPQUFMLENBQWFDLE9BQWxEO0FBRUEsUUFBTVksTUFBTSxHQUFHLE1BQU0sK0JBQWtCZixJQUFJLENBQUNFLE9BQUwsQ0FBYUMsT0FBL0IsRUFBd0NLLFVBQXhDLENBQXJCO0FBRUEsUUFBTVEsU0FBUyxHQUFHLElBQUlDLGVBQUosQ0FBVztBQUFDQyxJQUFBQSxXQUFXLEVBQUVDLFlBQUdDLElBQUgsR0FBVUMsTUFBVixHQUFtQjtBQUFqQyxHQUFYLENBQWxCO0FBRUEsUUFBTUMsR0FBRyxHQUFHLElBQUlDLHFCQUFKLENBQWlCWCxHQUFqQixFQUFzQkcsTUFBdEIsRUFBOEJDLFNBQTlCLENBQVo7QUFFQSxRQUFNUSxvQkFBb0IsR0FBRztBQUN6QixtQ0FBK0IsSUFBSUMsVUFBSixDQUFTVCxTQUFULEVBQzFCVSxHQUQwQixDQUN0QkMsbUJBRHNCLEVBRTFCRCxHQUYwQixDQUV0QkUseUJBRnNCLEVBRzFCRixHQUgwQixDQUd0QkcsdUJBSHNCLEVBSTFCSCxHQUowQixDQUl0QkkscUJBSnNCLENBRE47QUFNekIsZ0JBQVksSUFBSUwsVUFBSixDQUFTVCxTQUFULEVBQ1BVLEdBRE8sQ0FDSEMsbUJBREcsRUFFUEQsR0FGTyxDQUVISyxlQUZHLEVBR1BMLEdBSE8sQ0FHSE0sZUFIRyxFQUlQTixHQUpPLENBSUhJLHFCQUpHO0FBTmEsR0FBN0I7O0FBYUEsT0FBSyxNQUFNRyxDQUFYLElBQWdCbEIsTUFBTSxDQUFDbUIsV0FBdkIsRUFBb0M7QUFDaEMsVUFBTUMsVUFBVSxHQUFHcEIsTUFBTSxDQUFDbUIsV0FBUCxDQUFtQkQsQ0FBbkIsQ0FBbkI7QUFFQSw0QkFBT0UsVUFBVSxDQUFDQyxJQUFsQixFQUF3QkMsU0FBeEIsQ0FBa0NDLEtBQUssSUFBSTtBQUV2Q0EsTUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVdKLFVBQVgsR0FBd0JBLFVBQVUsQ0FBQ0ssSUFBbkM7O0FBRUEsWUFBTUMsbUJBQW1CLEdBQUdDLGdCQUFFQyxJQUFGLENBQU9uQixvQkFBUCxFQUE2QixDQUFDb0IsSUFBRCxFQUFPdkMsSUFBUCxLQUFnQjtBQUNyRSxlQUFPd0MsYUFBSUMsa0JBQUosQ0FBdUJSLEtBQUssQ0FBQ1MsSUFBN0IsRUFBbUMxQyxJQUFuQyxDQUFQO0FBQ0gsT0FGMkIsQ0FBNUI7O0FBSUEsVUFBSW9DLG1CQUFKLEVBQXlCQSxtQkFBbUIsQ0FBQ08sT0FBcEIsQ0FBNEIxQixHQUE1QixFQUFpQ2dCLEtBQWpDO0FBRTVCLEtBVkQ7QUFZSDs7QUFFRCxRQUFNdEIsU0FBUyxDQUFDaUMsTUFBVixFQUFOO0FBRUEsUUFBTUMsVUFBVSxHQUFHLElBQUl6QixVQUFKLENBQVNULFNBQVQsRUFDZFUsR0FEYyxDQUNWQyxtQkFEVSxFQUVkRCxHQUZjLENBRVZ5QixrQkFGVSxFQUdkekIsR0FIYyxDQUdWMEIsc0JBSFUsRUFJZDFCLEdBSmMsQ0FJVjJCLGtCQUpVLEVBS2QzQixHQUxjLENBS1Y0QixpQkFMVSxFQU1kNUIsR0FOYyxDQU1WLE9BQU82QixDQUFQLEVBQVVDLENBQVYsS0FBZ0I7QUFBRUEsSUFBQUEsQ0FBQyxDQUFDVCxJQUFGLEdBQVNVLGFBQU1DLElBQU4sQ0FBV0gsQ0FBQyxDQUFDSSxVQUFiLEVBQXlCSCxDQUFDLENBQUNqQixJQUFGLENBQU9xQixVQUFoQyxDQUFUO0FBQXNESixJQUFBQSxDQUFDLENBQUNLLE9BQUYsR0FBWSxLQUFaO0FBQW1CLFdBQU9MLENBQVA7QUFBVyxHQU41RixFQU9kOUIsR0FQYyxDQU9Wb0MsbUJBUFUsRUFRZHBDLEdBUmMsQ0FRVnFDLGlDQVJVLEVBU2RyQyxHQVRjLENBU1ZzQywrQkFUVSxDQUFuQjs7QUFXQSxPQUFLLE1BQU0vQixDQUFYLElBQWdCbEIsTUFBTSxDQUFDa0QsT0FBdkIsRUFBZ0M7QUFFNUIsVUFBTTNCLEtBQUssR0FBRyxzQkFBUW1CLGFBQU1DLElBQU4sQ0FBV3BDLEdBQUcsQ0FBQ25CLE9BQWYsRUFBd0JZLE1BQU0sQ0FBQ2tELE9BQVAsQ0FBZWhDLENBQWYsRUFBa0JpQyxHQUExQyxDQUFSLENBQWQ7QUFDQTVCLElBQUFBLEtBQUssQ0FBQ0MsSUFBTixDQUFXcUIsVUFBWCxHQUF3QnRCLEtBQUssQ0FBQzZCLElBQTlCO0FBQ0EsVUFBTWpCLFVBQVUsQ0FBQ0YsT0FBWCxDQUFtQjFCLEdBQW5CLEVBQXdCZ0IsS0FBeEIsQ0FBTjtBQUVIOztBQUVELE1BQUk4QixTQUFTLEdBQUc7QUFBRUMsSUFBQUEsT0FBTyxFQUFFO0FBQVgsR0FBaEI7QUFFQSxRQUFNQyxVQUFVLEdBQUdoRCxHQUFHLENBQUNpRCxrQkFBSixDQUF1QkMsWUFBdkIsRUFBbkI7O0FBRUEsT0FBSyxNQUFNdkMsQ0FBWCxJQUFnQnFDLFVBQWhCLEVBQTRCO0FBQ3hCLFVBQU1HLE1BQU0sR0FBR25ELEdBQUcsQ0FBQ2lELGtCQUFKLENBQXVCRyxXQUF2QixDQUFtQ0osVUFBVSxDQUFDckMsQ0FBRCxDQUE3QyxDQUFmO0FBRUFtQyxJQUFBQSxTQUFTLENBQUNDLE9BQVYsQ0FBa0JJLE1BQU0sQ0FBQ2xDLElBQVAsQ0FBWXFCLFVBQTlCLElBQTRDSCxhQUFNa0IsUUFBTixDQUFlckQsR0FBRyxDQUFDc0QsU0FBbkIsRUFBOEJILE1BQU0sQ0FBQzFCLElBQXJDLENBQTVDO0FBRUg7O0FBRUQsUUFBTXpCLEdBQUcsQ0FBQ3VELEVBQUosQ0FBT0MsZ0JBQVAsQ0FBd0JyQixhQUFNQyxJQUFOLENBQVdwQyxHQUFHLENBQUNxQyxVQUFmLEVBQTJCLGdCQUEzQixDQUF4QixFQUFzRW9CLElBQUksQ0FBQ0MsU0FBTCxDQUFlWixTQUFmLENBQXRFLENBQU47O0FBRUEsT0FBSyxNQUFNbkMsQ0FBWCxJQUFnQmxCLE1BQU0sQ0FBQ21CLFdBQXZCLEVBQW9DO0FBQ2hDLFVBQU1DLFVBQVUsR0FBR3BCLE1BQU0sQ0FBQ21CLFdBQVAsQ0FBbUJELENBQW5CLENBQW5COztBQUVBLFNBQUssTUFBTWdELENBQVgsSUFBZ0I5QyxVQUFVLENBQUMrQyxLQUEzQixFQUFrQztBQUM5QixZQUFNQyxJQUFJLEdBQUdoRCxVQUFVLENBQUMrQyxLQUFYLENBQWlCRCxDQUFqQixDQUFiO0FBRUEzRCxNQUFBQSxHQUFHLENBQUNWLEdBQUosQ0FBUXdFLEtBQVIsQ0FBYyxzQ0FBZCxFQUFzREQsSUFBSSxDQUFDakIsR0FBM0QsRUFBZ0UvQixVQUFVLENBQUNLLElBQTNFO0FBRUEsWUFBTTZDLE9BQU8sR0FBRy9ELEdBQUcsQ0FBQ2dFLGFBQUosQ0FBa0JuRCxVQUFVLENBQUNLLElBQTdCLEVBQW1DOUIsS0FBbkMsRUFBaEI7O0FBRUEsWUFBTTZFLFFBQVEsR0FBR0MsYUFBSUMsV0FBSixDQUFnQmhDLGFBQU1DLElBQU4sQ0FBV3BDLEdBQUcsQ0FBQ25CLE9BQWYsRUFBd0JnRixJQUFJLENBQUNqQixHQUE3QixDQUFoQixDQUFqQjs7QUFFQSxZQUFNd0Isa0JBQWtCLEdBQUcsSUFBSWpFLFVBQUosQ0FBU1QsU0FBVCxFQUN0QlUsR0FEc0IsQ0FDbEJpRSxrQkFEa0IsRUFFdEJqRSxHQUZzQixDQUVsQixvQkFBVTZELFFBQVYsQ0FGa0IsRUFHdEI3RCxHQUhzQixDQUdsQm9DLG1CQUhrQixFQUl0QnBDLEdBSnNCLENBSWxCcUMsaUNBSmtCLEVBS3RCckMsR0FMc0IsQ0FLbEJzQywrQkFMa0IsQ0FBM0I7O0FBT0EsV0FBSyxNQUFNNEIsQ0FBWCxJQUFnQlAsT0FBaEIsRUFBeUI7QUFDckIsY0FBTVEsS0FBSyxHQUFHbkQsZ0JBQUVvRCxTQUFGLENBQVlULE9BQU8sQ0FBQ08sQ0FBRCxDQUFuQixDQUFkOztBQUNBQyxRQUFBQSxLQUFLLENBQUN0RCxJQUFOLENBQVd3RCxTQUFYLEdBQXVCLDZCQUFlWixJQUFJLENBQUNhLEdBQXBCLEVBQXlCSCxLQUFLLENBQUN0RCxJQUEvQixDQUF2QjtBQUVBLFlBQUk0QyxJQUFJLENBQUNjLFNBQVQsRUFBb0JaLE9BQU8sQ0FBQ08sQ0FBRCxDQUFQLENBQVdyRCxJQUFYLENBQWdCd0QsU0FBaEIsR0FBNEJGLEtBQUssQ0FBQ3RELElBQU4sQ0FBV3dELFNBQXZDO0FBRXBCRixRQUFBQSxLQUFLLENBQUM5QyxJQUFOLEdBQWFVLGFBQU1DLElBQU4sQ0FBV3BDLEdBQUcsQ0FBQ3NELFNBQWYsRUFBMEJsQyxnQkFBRXdELFNBQUYsQ0FBWUwsS0FBSyxDQUFDdEQsSUFBTixDQUFXd0QsU0FBdkIsRUFBa0MsR0FBbEMsQ0FBMUIsQ0FBYjtBQUVBLGNBQU1MLGtCQUFrQixDQUFDMUMsT0FBbkIsQ0FBMkIxQixHQUEzQixFQUFnQ3VFLEtBQWhDLENBQU47QUFDSDtBQUVKO0FBRUo7O0FBRUQsT0FBSyxNQUFNNUQsQ0FBWCxJQUFnQmxCLE1BQU0sQ0FBQ29GLEtBQXZCLEVBQThCO0FBRTFCLFVBQU1DLGNBQWMsR0FBRyxJQUFJM0UsVUFBSixDQUFTVCxTQUFULENBQXZCO0FBRUEsVUFBTXFGLElBQUksR0FBR3RGLE1BQU0sQ0FBQ29GLEtBQVAsQ0FBYWxFLENBQWIsQ0FBYjtBQUVBLFFBQUlLLEtBQUssR0FBRyxDQUFDLENBQUMrRCxJQUFJLENBQUNDLFFBQVAsR0FBa0Isc0JBQVE3QyxhQUFNQyxJQUFOLENBQVdwQyxHQUFHLENBQUNuQixPQUFmLEVBQXdCa0csSUFBSSxDQUFDQyxRQUE3QixDQUFSLENBQWxCLEdBQW9FLElBQUlDLFlBQUosRUFBaEY7O0FBQ0EsVUFBTWhCLFFBQVEsR0FBR0MsYUFBSUMsV0FBSixDQUFnQmhDLGFBQU1DLElBQU4sQ0FBV3BDLEdBQUcsQ0FBQ25CLE9BQWYsRUFBd0JrRyxJQUFJLENBQUNkLFFBQTdCLENBQWhCLENBQWpCOztBQUVBakQsSUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVd3RCxTQUFYLEdBQXVCTSxJQUFJLENBQUNMLEdBQTVCOztBQUVBLFFBQUksQ0FBQyxDQUFDSyxJQUFJLENBQUNDLFFBQVgsRUFBcUI7QUFDakJGLE1BQUFBLGNBQWMsQ0FDVDFFLEdBREwsQ0FDU0MsbUJBRFQsRUFFS0QsR0FGTCxDQUVTRSx5QkFGVDtBQUdDOztBQUVMd0UsSUFBQUEsY0FBYyxDQUNUMUUsR0FETCxDQUNTRyx1QkFEVCxFQUVLSCxHQUZMLENBRVNJLHFCQUZULEVBR0tKLEdBSEwsQ0FHUyxPQUFPNkIsQ0FBUCxFQUFVQyxDQUFWLEtBQWdCO0FBQUVBLE1BQUFBLENBQUMsQ0FBQ1QsSUFBRixHQUFTVSxhQUFNQyxJQUFOLENBQVdILENBQUMsQ0FBQ3FCLFNBQWIsRUFBd0JsQyxnQkFBRXdELFNBQUYsQ0FBWTFDLENBQUMsQ0FBQ2pCLElBQUYsQ0FBT3dELFNBQW5CLEVBQThCLEdBQTlCLENBQXhCLENBQVQ7QUFBc0UsYUFBT3ZDLENBQVA7QUFBVyxLQUg1RyxFQUlLOUIsR0FKTCxDQUlTLG9CQUFVNkQsUUFBVixDQUpULEVBS0s3RCxHQUxMLENBS1NvQyxtQkFMVCxFQU1LcEMsR0FOTCxDQU1TcUMsaUNBTlQsRUFPS3JDLEdBUEwsQ0FPU3NDLCtCQVBUO0FBU0EsVUFBTW9DLGNBQWMsQ0FBQ3BELE9BQWYsQ0FBdUIxQixHQUF2QixFQUE0QmdCLEtBQTVCLENBQU47QUFFSDs7QUFFRCxRQUFNdEIsU0FBUyxDQUFDaUMsTUFBVixFQUFOO0FBR0MsQ0FqS0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBwYXJzZSB9IGZyb20gJ0Bib29zdC9hcmdzJ1xuaW1wb3J0IHsgY3JlYXRlTG9nZ2VyIH0gZnJvbSAnLi91dGlsL0xvZydcbmltcG9ydCB7IGZpbmRCYXNlRGlyLCBsb2FkQ29uZmlndXJhdGlvbiB9IGZyb20gJy4vQ29uZmlnJ1xuaW1wb3J0IHsgVW1hbWlDb250ZXh0IH0gZnJvbSAnLi9Db250ZXh0J1xuaW1wb3J0IHsgeGZzLCBwcGF0aCB9IGZyb20gJ0B5YXJucGtnL2ZzbGliJ1xuXG5pbXBvcnQgeyBWRmlsZSB9IGZyb20gJ3ZmaWxlJ1xuaW1wb3J0IHRvVkZpbGUgZnJvbSAndG8tdmZpbGUnXG5pbXBvcnQgdmZHbG9iIGZyb20gJ3ZmaWxlLWdsb2InXG5pbXBvcnQgaW50ZXJwb2xhdGVVcmwgZnJvbSAnaW50ZXJwb2xhdGUtdXJsJ1xuaW1wb3J0IHBRdWV1ZSBmcm9tICdwLXF1ZXVlJ1xuXG5cbmltcG9ydCB7IGxvYWRDb250ZW50LCBmbnZIYXNoLCBzYXZlQ29udGVudCwgYnJvdGxpQ29tcHJlc3NUcmFuc3BhcmVudCwgZ3ppcENvbXByZXNzVHJhbnNwYXJlbnQgfSBmcm9tICcuL3Rhc2tzL0NvbW1vbidcbmltcG9ydCB7IHBhcnNlQmFiZWwsIGV4dHJhY3RJbXBvcnRzLCBpbXBvcnRUcmVlLCB0cmFuc3BpbGUgfSBmcm9tICcuL3Rhc2tzL1NjcmlwdCdcbmltcG9ydCB7IHRyYW5zZm9ybU1hcmtkb3duLCBwb3N0RnJvbnRtYXR0ZXIgfSBmcm9tICcuL3Rhc2tzL1JlbWFyaydcbmltcG9ydCB7IHJlbmRlclB1ZyB9IGZyb20gJy4vdGFza3MvUHVnJ1xuaW1wb3J0IHsgcGFyc2VZYW1sLCByZXNvbHZlWHJlZnMgfSBmcm9tICcuL3Rhc2tzL1lhbWwnXG5pbXBvcnQgeyBwcm9jZXNzQXNzZXRzIH0gZnJvbSAnLi90YXNrcy9Bc3NldHMnXG5cbmltcG9ydCB7IEZsb3cgfSBmcm9tICcuL3V0aWwvRmxvdydcbmltcG9ydCBVVEkgZnJvbSAnLi91dGlsL1VUSSdcblxuaW1wb3J0IG9zIGZyb20gJ29zJ1xuaW1wb3J0IHB1ZyBmcm9tICdwdWcnXG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnXG5cbihhc3luYyAoKSA9PiB7XG5cbmNvbnN0IGFyZ3YgPSBwcm9jZXNzLmFyZ3Yuc2xpY2UoMilcblxuY29uc3QgYXJncyA9IHBhcnNlPHt9Pihhcmd2LCB7XG4gICAgY29tbWFuZHM6IFsnYnVpbGQnXSxcbiAgICBvcHRpb25zOiB7XG4gICAgICAgIGJhc2VEaXI6IHtcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnQmFzZSBkaXJlY3Rvcnkgb2YgdGhlIHNpdGUgdG8gYnVpbGQnLFxuICAgICAgICAgICAgdHlwZTogJ3N0cmluZycsXG4gICAgICAgICAgICBzaG9ydDogJ0MnLFxuICAgICAgICAgICAgZGVmYXVsdDogYXdhaXQgZmluZEJhc2VEaXIoeGZzKSxcbiAgICAgICAgICAgIHZhbGlkYXRlICh2YWx1ZSkgeyByZXR1cm4geGZzLmV4aXN0c1N5bmModmFsdWUpIH1cbiAgICAgICAgfVxuICAgIH1cbn0pXG5cblxuY29uc3QgbG9nID0gY3JlYXRlTG9nZ2VyKClcbmxvZy5sZXZlbCA9ICd0cmFjZSdcblxubG9nLmluZm8oXCJVbWFtaSAtIGEgc2F2b3VyeSBzdGF0aWMgc2l0ZSBnZW5lcmF0b3JcIilcbmxvZy5pbmZvKFwiVXNpbmcgYmFzZSBkaXJlY3Rvcnk6ICVzXCIsIGFyZ3Mub3B0aW9ucy5iYXNlRGlyKVxuXG5jb25zdCBjb25maWcgPSBhd2FpdCBsb2FkQ29uZmlndXJhdGlvbihhcmdzLm9wdGlvbnMuYmFzZURpciwgeGZzKVxuXG5jb25zdCB3b3JrUXVldWUgPSBuZXcgcFF1ZXVlKHtjb25jdXJyZW5jeTogb3MuY3B1cygpLmxlbmd0aCArIDF9KVxuXG5jb25zdCBjdHggPSBuZXcgVW1hbWlDb250ZXh0KGxvZywgY29uZmlnLCB3b3JrUXVldWUpXG5cbmNvbnN0IGNvbGxlY3Rpb25FbnRyeUZsb3dzID0ge1xuICAgICduZXQuZGFyaW5nZmlyZWJhbGwubWFya2Rvd24nOiBuZXcgRmxvdyh3b3JrUXVldWUpXG4gICAgICAgIC5hZGQobG9hZENvbnRlbnQpXG4gICAgICAgIC5hZGQodHJhbnNmb3JtTWFya2Rvd24pXG4gICAgICAgIC5hZGQocG9zdEZyb250bWF0dGVyKVxuICAgICAgICAuYWRkKHByb2Nlc3NBc3NldHMpLFxuICAgICdvcmcueWFtbCc6IG5ldyBGbG93KHdvcmtRdWV1ZSlcbiAgICAgICAgLmFkZChsb2FkQ29udGVudClcbiAgICAgICAgLmFkZChmbnZIYXNoKVxuICAgICAgICAuYWRkKHBhcnNlWWFtbClcbiAgICAgICAgLmFkZChwcm9jZXNzQXNzZXRzKVxuICAgIH1cblxuZm9yIChjb25zdCBpIGluIGNvbmZpZy5jb2xsZWN0aW9ucykge1xuICAgIGNvbnN0IGNvbGxlY3Rpb24gPSBjb25maWcuY29sbGVjdGlvbnNbaV1cblxuICAgIHZmR2xvYihjb2xsZWN0aW9uLmdsb2IpLnN1YnNjcmliZSh2ZmlsZSA9PiB7XG5cbiAgICAgICAgdmZpbGUuZGF0YS5jb2xsZWN0aW9uID0gY29sbGVjdGlvbi5uYW1lO1xuXG4gICAgICAgIGNvbnN0IGNvbGxlY3Rpb25FbnRyeUZsb3cgPSBfLmZpbmQoY29sbGVjdGlvbkVudHJ5Rmxvd3MsIChmbG93LCB0eXBlKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gVVRJLmZpbGVOYW1lQ29uZm9ybXNUbyh2ZmlsZS5wYXRoLCB0eXBlKVxuICAgICAgICB9KVxuXG4gICAgICAgIGlmIChjb2xsZWN0aW9uRW50cnlGbG93KSBjb2xsZWN0aW9uRW50cnlGbG93LmV4ZWN1dGUoY3R4LCB2ZmlsZSk7XG5cbiAgICB9KVxuXG59XG5cbmF3YWl0IHdvcmtRdWV1ZS5vbklkbGUoKVxuXG5jb25zdCBzY3JpcHRGbG93ID0gbmV3IEZsb3cod29ya1F1ZXVlKVxuICAgIC5hZGQobG9hZENvbnRlbnQpXG4gICAgLmFkZChwYXJzZUJhYmVsKVxuICAgIC5hZGQoZXh0cmFjdEltcG9ydHMpXG4gICAgLmFkZChpbXBvcnRUcmVlKVxuICAgIC5hZGQodHJhbnNwaWxlKVxuICAgIC5hZGQoYXN5bmMgKGMsIHYpID0+IHsgdi5wYXRoID0gcHBhdGguam9pbihjLnNjcmlwdHNEaXIsIHYuZGF0YS5tb2R1bGVOYW1lKTsgdi5leHRuYW1lID0gJy5qcyc7IHJldHVybiB2OyB9KVxuICAgIC5hZGQoc2F2ZUNvbnRlbnQpXG4gICAgLmFkZChicm90bGlDb21wcmVzc1RyYW5zcGFyZW50KVxuICAgIC5hZGQoZ3ppcENvbXByZXNzVHJhbnNwYXJlbnQpXG5cbmZvciAoY29uc3QgaSBpbiBjb25maWcuc2NyaXB0cykge1xuXG4gICAgY29uc3QgdmZpbGUgPSB0b1ZGaWxlKHBwYXRoLmpvaW4oY3R4LmJhc2VEaXIsIGNvbmZpZy5zY3JpcHRzW2ldLnNyYykpXG4gICAgdmZpbGUuZGF0YS5tb2R1bGVOYW1lID0gdmZpbGUuc3RlbVxuICAgIGF3YWl0IHNjcmlwdEZsb3cuZXhlY3V0ZShjdHgsIHZmaWxlKVxuXG59XG5cbmxldCBpbXBvcnRNYXAgPSB7IGltcG9ydHM6IHt9fVxuXG5jb25zdCBhbGxTY3JpcHRzID0gY3R4LnNjcmlwdERlcGVuZGVuY2llcy5vdmVyYWxsT3JkZXIoKVxuXG5mb3IgKGNvbnN0IGkgaW4gYWxsU2NyaXB0cykge1xuICAgIGNvbnN0IHNjcmlwdCA9IGN0eC5zY3JpcHREZXBlbmRlbmNpZXMuZ2V0Tm9kZURhdGEoYWxsU2NyaXB0c1tpXSlcblxuICAgIGltcG9ydE1hcC5pbXBvcnRzW3NjcmlwdC5kYXRhLm1vZHVsZU5hbWVdID0gcHBhdGgucmVsYXRpdmUoY3R4Lm91dHB1dERpciwgc2NyaXB0LnBhdGgpXG5cbn1cblxuYXdhaXQgY3R4LmZzLndyaXRlRmlsZVByb21pc2UocHBhdGguam9pbihjdHguc2NyaXB0c0RpciwgJ2ltcG9ydG1hcC5qc29uJyksIEpTT04uc3RyaW5naWZ5KGltcG9ydE1hcCkpXG5cbmZvciAoY29uc3QgaSBpbiBjb25maWcuY29sbGVjdGlvbnMpIHtcbiAgICBjb25zdCBjb2xsZWN0aW9uID0gY29uZmlnLmNvbGxlY3Rpb25zW2ldXG5cbiAgICBmb3IgKGNvbnN0IGogaW4gY29sbGVjdGlvbi52aWV3cykge1xuICAgICAgICBjb25zdCB2aWV3ID0gY29sbGVjdGlvbi52aWV3c1tqXVxuXG4gICAgICAgIGN0eC5sb2cudHJhY2UoXCJQcm9jZXNzaW5nIHZpZXcgJXMgZm9yIGNvbGxlY3Rpb24gJXNcIiwgdmlldy5zcmMsIGNvbGxlY3Rpb24ubmFtZSlcblxuICAgICAgICBjb25zdCBlbnRyaWVzID0gY3R4LmdldENvbGxlY3Rpb24oY29sbGVjdGlvbi5uYW1lKS52YWx1ZSgpXG5cbiAgICAgICAgY29uc3QgdGVtcGxhdGUgPSBwdWcuY29tcGlsZUZpbGUocHBhdGguam9pbihjdHguYmFzZURpciwgdmlldy5zcmMpKVxuXG4gICAgICAgIGNvbnN0IGNvbGxlY3Rpb25WaWV3RmxvdyA9IG5ldyBGbG93KHdvcmtRdWV1ZSlcbiAgICAgICAgICAgIC5hZGQocmVzb2x2ZVhyZWZzKVxuICAgICAgICAgICAgLmFkZChyZW5kZXJQdWcodGVtcGxhdGUpKVxuICAgICAgICAgICAgLmFkZChzYXZlQ29udGVudClcbiAgICAgICAgICAgIC5hZGQoYnJvdGxpQ29tcHJlc3NUcmFuc3BhcmVudClcbiAgICAgICAgICAgIC5hZGQoZ3ppcENvbXByZXNzVHJhbnNwYXJlbnQpXG5cbiAgICAgICAgZm9yIChjb25zdCBrIGluIGVudHJpZXMpIHtcbiAgICAgICAgICAgIGNvbnN0IGVudHJ5ID0gXy5jbG9uZURlZXAoZW50cmllc1trXSlcbiAgICAgICAgICAgIGVudHJ5LmRhdGEucGVybWFsaW5rID0gaW50ZXJwb2xhdGVVcmwodmlldy51cmksIGVudHJ5LmRhdGEpXG5cbiAgICAgICAgICAgIGlmICh2aWV3LmNhbm9uaWNhbCkgZW50cmllc1trXS5kYXRhLnBlcm1hbGluayA9IGVudHJ5LmRhdGEucGVybWFsaW5rXG5cbiAgICAgICAgICAgIGVudHJ5LnBhdGggPSBwcGF0aC5qb2luKGN0eC5vdXRwdXREaXIsIF8udHJpbVN0YXJ0KGVudHJ5LmRhdGEucGVybWFsaW5rLCAnLycpKVxuXG4gICAgICAgICAgICBhd2FpdCBjb2xsZWN0aW9uVmlld0Zsb3cuZXhlY3V0ZShjdHgsIGVudHJ5KTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG59XG5cbmZvciAoY29uc3QgaSBpbiBjb25maWcucGFnZXMpIHtcblxuICAgIGNvbnN0IHBhZ2VSZW5kZXJGbG93ID0gbmV3IEZsb3cod29ya1F1ZXVlKVxuXG4gICAgY29uc3QgcGFnZSA9IGNvbmZpZy5wYWdlc1tpXVxuXG4gICAgbGV0IHZmaWxlID0gISFwYWdlLmNvbnRlbnRzID8gdG9WRmlsZShwcGF0aC5qb2luKGN0eC5iYXNlRGlyLCBwYWdlLmNvbnRlbnRzKSkgOiBuZXcgVkZpbGUoKTtcbiAgICBjb25zdCB0ZW1wbGF0ZSA9IHB1Zy5jb21waWxlRmlsZShwcGF0aC5qb2luKGN0eC5iYXNlRGlyLCBwYWdlLnRlbXBsYXRlKSlcblxuICAgIHZmaWxlLmRhdGEucGVybWFsaW5rID0gcGFnZS51cmlcblxuICAgIGlmICghIXBhZ2UuY29udGVudHMpIHtcbiAgICAgICAgcGFnZVJlbmRlckZsb3dcbiAgICAgICAgICAgIC5hZGQobG9hZENvbnRlbnQpXG4gICAgICAgICAgICAuYWRkKHRyYW5zZm9ybU1hcmtkb3duKVxuICAgICAgICB9XG5cbiAgICBwYWdlUmVuZGVyRmxvd1xuICAgICAgICAuYWRkKHBvc3RGcm9udG1hdHRlcilcbiAgICAgICAgLmFkZChwcm9jZXNzQXNzZXRzKVxuICAgICAgICAuYWRkKGFzeW5jIChjLCB2KSA9PiB7IHYucGF0aCA9IHBwYXRoLmpvaW4oYy5vdXRwdXREaXIsIF8udHJpbVN0YXJ0KHYuZGF0YS5wZXJtYWxpbmssICcvJykpOyByZXR1cm4gdjsgfSlcbiAgICAgICAgLmFkZChyZW5kZXJQdWcodGVtcGxhdGUpKVxuICAgICAgICAuYWRkKHNhdmVDb250ZW50KVxuICAgICAgICAuYWRkKGJyb3RsaUNvbXByZXNzVHJhbnNwYXJlbnQpXG4gICAgICAgIC5hZGQoZ3ppcENvbXByZXNzVHJhbnNwYXJlbnQpXG5cbiAgICBhd2FpdCBwYWdlUmVuZGVyRmxvdy5leGVjdXRlKGN0eCwgdmZpbGUpXG5cbn1cblxuYXdhaXQgd29ya1F1ZXVlLm9uSWRsZSgpXG5cblxufSkoKSJdfQ==