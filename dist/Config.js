"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findBaseDir = findBaseDir;
exports.loadConfiguration = loadConfiguration;

var _optimal = _interopRequireWildcard(require("optimal"));

var _fslib = require("@yarnpkg/fslib");

var _vm = require("vm2");

var _fault = _interopRequireDefault(require("fault"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const bp_page_spec = {
  uri: (0, _optimal.string)().required().notEmpty(),
  contents: (0, _optimal.string)().notEmpty(),
  template: (0, _optimal.string)().required().notEmpty()
};
const bp_asset_spec = {
  uri: (0, _optimal.string)().required().notEmpty(),
  src: (0, _optimal.string)().required().notEmpty()
};
const bp_view_spec = {
  uri: (0, _optimal.string)().required().notEmpty(),
  src: (0, _optimal.string)().required().notEmpty(),
  canonical: (0, _optimal.bool)()
};
const bp_collection_spec = {
  name: (0, _optimal.string)().required().camelCase().notEmpty(),
  glob: (0, _optimal.string)().required().notEmpty(),
  views: (0, _optimal.array)((0, _optimal.shape)(bp_view_spec)),
  xrefs: (0, _optimal.object)()
};
const bp_settings_spec = {
  dirs: (0, _optimal.shape)({
    base: (0, _optimal.string)(),
    output: (0, _optimal.string)(),
    scripts: (0, _optimal.string)(),
    styles: (0, _optimal.string)()
  }),
  markdown: (0, _optimal.shape)({
    classes: (0, _optimal.object)()
  })
};
const blueprint = {
  collections: (0, _optimal.array)((0, _optimal.shape)(bp_collection_spec)),
  pages: (0, _optimal.array)((0, _optimal.shape)(bp_page_spec)),
  scripts: (0, _optimal.array)((0, _optimal.shape)(bp_asset_spec)),
  stylesheets: (0, _optimal.array)((0, _optimal.shape)(bp_asset_spec)),
  assets: (0, _optimal.array)((0, _optimal.shape)(bp_asset_spec)),
  site: (0, _optimal.object)(),
  settings: (0, _optimal.shape)(bp_settings_spec)
};

async function findBaseDir(fs) {
  let currentDir = process.cwd();

  while (!(await fs.existsPromise(_fslib.ppath.join(currentDir, 'umami.config.js')))) {
    console.log(`Testing for baseDir: ${currentDir}`);
    if (currentDir == '/') throw (0, _fault.default)("Could not determine base directory. No umami.config.js file found.");
    currentDir = _fslib.ppath.dirname(currentDir);
  }

  return currentDir;
}

async function loadConfiguration(baseDir, fs) {
  const existsJs = await fs.existsPromise(_fslib.ppath.join(baseDir, 'umami.config.js'));

  if (existsJs) {
    var _loaded$settings$dirs, _loaded$settings$dirs2, _loaded$settings$dirs3, _loaded$settings$dirs4, _loaded$settings$dirs5, _loaded$settings;

    const configScriptSrc = await fs.readFilePromise(_fslib.ppath.join(baseDir, 'umami.config.js'));
    const configScript = new _vm.VMScript(configScriptSrc);
    const vm = new _vm.NodeVM();
    const loaded = (0, _optimal.default)(vm.run(configScript, _fslib.ppath.join(baseDir, 'umami.config.js')), blueprint);
    (_loaded$settings$dirs = loaded.settings.dirs).base || (_loaded$settings$dirs.base = baseDir);
    (_loaded$settings$dirs2 = loaded.settings.dirs).output || (_loaded$settings$dirs2.output = _fslib.ppath.join(loaded.settings.dirs.base, 'dist'));
    (_loaded$settings$dirs3 = loaded.settings.dirs).scripts || (_loaded$settings$dirs3.scripts = _fslib.ppath.join(loaded.settings.dirs.output, 'scripts'));
    (_loaded$settings$dirs4 = loaded.settings.dirs).styles || (_loaded$settings$dirs4.styles = _fslib.ppath.join(loaded.settings.dirs.output, 'styles'));
    (_loaded$settings$dirs5 = loaded.settings.dirs).assets || (_loaded$settings$dirs5.assets = _fslib.ppath.join(loaded.settings.dirs.output, 'assets'));
    (_loaded$settings = loaded.settings).markdown || (_loaded$settings.markdown = {});
    return loaded;
  }

  return (0, _optimal.default)({}, blueprint);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Db25maWcudHMiXSwibmFtZXMiOlsiYnBfcGFnZV9zcGVjIiwidXJpIiwicmVxdWlyZWQiLCJub3RFbXB0eSIsImNvbnRlbnRzIiwidGVtcGxhdGUiLCJicF9hc3NldF9zcGVjIiwic3JjIiwiYnBfdmlld19zcGVjIiwiY2Fub25pY2FsIiwiYnBfY29sbGVjdGlvbl9zcGVjIiwibmFtZSIsImNhbWVsQ2FzZSIsImdsb2IiLCJ2aWV3cyIsInhyZWZzIiwiYnBfc2V0dGluZ3Nfc3BlYyIsImRpcnMiLCJiYXNlIiwib3V0cHV0Iiwic2NyaXB0cyIsInN0eWxlcyIsIm1hcmtkb3duIiwiY2xhc3NlcyIsImJsdWVwcmludCIsImNvbGxlY3Rpb25zIiwicGFnZXMiLCJzdHlsZXNoZWV0cyIsImFzc2V0cyIsInNpdGUiLCJzZXR0aW5ncyIsImZpbmRCYXNlRGlyIiwiZnMiLCJjdXJyZW50RGlyIiwicHJvY2VzcyIsImN3ZCIsImV4aXN0c1Byb21pc2UiLCJwcGF0aCIsImpvaW4iLCJjb25zb2xlIiwibG9nIiwiZGlybmFtZSIsImxvYWRDb25maWd1cmF0aW9uIiwiYmFzZURpciIsImV4aXN0c0pzIiwiY29uZmlnU2NyaXB0U3JjIiwicmVhZEZpbGVQcm9taXNlIiwiY29uZmlnU2NyaXB0IiwiVk1TY3JpcHQiLCJ2bSIsIk5vZGVWTSIsImxvYWRlZCIsInJ1biJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7QUFJQSxNQUFNQSxZQUFZLEdBQUc7QUFDakJDLEVBQUFBLEdBQUcsRUFBRSx1QkFBU0MsUUFBVCxHQUFvQkMsUUFBcEIsRUFEWTtBQUVqQkMsRUFBQUEsUUFBUSxFQUFFLHVCQUFTRCxRQUFULEVBRk87QUFHakJFLEVBQUFBLFFBQVEsRUFBRSx1QkFBU0gsUUFBVCxHQUFvQkMsUUFBcEI7QUFITyxDQUFyQjtBQU1BLE1BQU1HLGFBQWEsR0FBRztBQUNsQkwsRUFBQUEsR0FBRyxFQUFFLHVCQUFTQyxRQUFULEdBQW9CQyxRQUFwQixFQURhO0FBRWxCSSxFQUFBQSxHQUFHLEVBQUUsdUJBQVNMLFFBQVQsR0FBb0JDLFFBQXBCO0FBRmEsQ0FBdEI7QUFLQSxNQUFNSyxZQUFZLEdBQUc7QUFDakJQLEVBQUFBLEdBQUcsRUFBRSx1QkFBU0MsUUFBVCxHQUFvQkMsUUFBcEIsRUFEWTtBQUVqQkksRUFBQUEsR0FBRyxFQUFFLHVCQUFTTCxRQUFULEdBQW9CQyxRQUFwQixFQUZZO0FBR2pCTSxFQUFBQSxTQUFTLEVBQUU7QUFITSxDQUFyQjtBQU1BLE1BQU1DLGtCQUFrQixHQUFHO0FBQ3ZCQyxFQUFBQSxJQUFJLEVBQUUsdUJBQVNULFFBQVQsR0FBb0JVLFNBQXBCLEdBQWdDVCxRQUFoQyxFQURpQjtBQUV2QlUsRUFBQUEsSUFBSSxFQUFFLHVCQUFTWCxRQUFULEdBQW9CQyxRQUFwQixFQUZpQjtBQUd2QlcsRUFBQUEsS0FBSyxFQUFFLG9CQUFNLG9CQUFNTixZQUFOLENBQU4sQ0FIZ0I7QUFJdkJPLEVBQUFBLEtBQUssRUFBRTtBQUpnQixDQUEzQjtBQU9BLE1BQU1DLGdCQUFnQixHQUFHO0FBQ3JCQyxFQUFBQSxJQUFJLEVBQUUsb0JBQU07QUFDUkMsSUFBQUEsSUFBSSxFQUFFLHNCQURFO0FBRVJDLElBQUFBLE1BQU0sRUFBRSxzQkFGQTtBQUdSQyxJQUFBQSxPQUFPLEVBQUUsc0JBSEQ7QUFJUkMsSUFBQUEsTUFBTSxFQUFFO0FBSkEsR0FBTixDQURlO0FBT3JCQyxFQUFBQSxRQUFRLEVBQUUsb0JBQU07QUFDWkMsSUFBQUEsT0FBTyxFQUFFO0FBREcsR0FBTjtBQVBXLENBQXpCO0FBWUEsTUFBTUMsU0FBUyxHQUFHO0FBQ2RDLEVBQUFBLFdBQVcsRUFBRSxvQkFBTSxvQkFBTWYsa0JBQU4sQ0FBTixDQURDO0FBRWRnQixFQUFBQSxLQUFLLEVBQUUsb0JBQU0sb0JBQU0xQixZQUFOLENBQU4sQ0FGTztBQUdkb0IsRUFBQUEsT0FBTyxFQUFFLG9CQUFNLG9CQUFNZCxhQUFOLENBQU4sQ0FISztBQUlkcUIsRUFBQUEsV0FBVyxFQUFFLG9CQUFNLG9CQUFNckIsYUFBTixDQUFOLENBSkM7QUFLZHNCLEVBQUFBLE1BQU0sRUFBRSxvQkFBTSxvQkFBTXRCLGFBQU4sQ0FBTixDQUxNO0FBTWR1QixFQUFBQSxJQUFJLEVBQUUsc0JBTlE7QUFPZEMsRUFBQUEsUUFBUSxFQUFFLG9CQUFNZCxnQkFBTjtBQVBJLENBQWxCOztBQTZDTyxlQUFlZSxXQUFmLENBQTRCQyxFQUE1QixFQUFvRTtBQUV2RSxNQUFJQyxVQUFVLEdBQUdDLE9BQU8sQ0FBQ0MsR0FBUixFQUFqQjs7QUFFQSxTQUFPLEVBQUMsTUFBTUgsRUFBRSxDQUFDSSxhQUFILENBQWlCQyxhQUFNQyxJQUFOLENBQVdMLFVBQVgsRUFBdUIsaUJBQXZCLENBQWpCLENBQVAsQ0FBUCxFQUEyRTtBQUN2RU0sSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQWEsd0JBQXVCUCxVQUFXLEVBQS9DO0FBQ0EsUUFBSUEsVUFBVSxJQUFJLEdBQWxCLEVBQXVCLE1BQU0sb0JBQU0sb0VBQU4sQ0FBTjtBQUN2QkEsSUFBQUEsVUFBVSxHQUFHSSxhQUFNSSxPQUFOLENBQWNSLFVBQWQsQ0FBYjtBQUNIOztBQUVELFNBQU9BLFVBQVA7QUFFSDs7QUFFTSxlQUFlUyxpQkFBZixDQUFrQ0MsT0FBbEMsRUFBeURYLEVBQXpELEVBQWtHO0FBRXJHLFFBQU1ZLFFBQVEsR0FBRyxNQUFNWixFQUFFLENBQUNJLGFBQUgsQ0FBaUJDLGFBQU1DLElBQU4sQ0FBV0ssT0FBWCxFQUFvQixpQkFBcEIsQ0FBakIsQ0FBdkI7O0FBRUEsTUFBSUMsUUFBSixFQUFjO0FBQUE7O0FBQ1YsVUFBTUMsZUFBZSxHQUFHLE1BQU1iLEVBQUUsQ0FBQ2MsZUFBSCxDQUFtQlQsYUFBTUMsSUFBTixDQUFXSyxPQUFYLEVBQW9CLGlCQUFwQixDQUFuQixDQUE5QjtBQUNBLFVBQU1JLFlBQVksR0FBRyxJQUFJQyxZQUFKLENBQWFILGVBQWIsQ0FBckI7QUFDQSxVQUFNSSxFQUFFLEdBQUcsSUFBSUMsVUFBSixFQUFYO0FBRUEsVUFBTUMsTUFBTSxHQUFHLHNCQUFRRixFQUFFLENBQUNHLEdBQUgsQ0FBT0wsWUFBUCxFQUFxQlYsYUFBTUMsSUFBTixDQUFXSyxPQUFYLEVBQW9CLGlCQUFwQixDQUFyQixDQUFSLEVBQXNFbkIsU0FBdEUsQ0FBZjtBQUNBLDZCQUFBMkIsTUFBTSxDQUFDckIsUUFBUCxDQUFnQmIsSUFBaEIsRUFBcUJDLElBQXJCLDJCQUFxQkEsSUFBckIsR0FBOEJ5QixPQUE5QjtBQUNBLDhCQUFBUSxNQUFNLENBQUNyQixRQUFQLENBQWdCYixJQUFoQixFQUFxQkUsTUFBckIsNEJBQXFCQSxNQUFyQixHQUFnQ2tCLGFBQU1DLElBQU4sQ0FBV2EsTUFBTSxDQUFDckIsUUFBUCxDQUFnQmIsSUFBaEIsQ0FBcUJDLElBQWhDLEVBQXNDLE1BQXRDLENBQWhDO0FBQ0EsOEJBQUFpQyxNQUFNLENBQUNyQixRQUFQLENBQWdCYixJQUFoQixFQUFxQkcsT0FBckIsNEJBQXFCQSxPQUFyQixHQUFpQ2lCLGFBQU1DLElBQU4sQ0FBV2EsTUFBTSxDQUFDckIsUUFBUCxDQUFnQmIsSUFBaEIsQ0FBcUJFLE1BQWhDLEVBQXdDLFNBQXhDLENBQWpDO0FBQ0EsOEJBQUFnQyxNQUFNLENBQUNyQixRQUFQLENBQWdCYixJQUFoQixFQUFxQkksTUFBckIsNEJBQXFCQSxNQUFyQixHQUFnQ2dCLGFBQU1DLElBQU4sQ0FBV2EsTUFBTSxDQUFDckIsUUFBUCxDQUFnQmIsSUFBaEIsQ0FBcUJFLE1BQWhDLEVBQXdDLFFBQXhDLENBQWhDO0FBQ0EsOEJBQUFnQyxNQUFNLENBQUNyQixRQUFQLENBQWdCYixJQUFoQixFQUFxQlcsTUFBckIsNEJBQXFCQSxNQUFyQixHQUFnQ1MsYUFBTUMsSUFBTixDQUFXYSxNQUFNLENBQUNyQixRQUFQLENBQWdCYixJQUFoQixDQUFxQkUsTUFBaEMsRUFBd0MsUUFBeEMsQ0FBaEM7QUFFQSx3QkFBQWdDLE1BQU0sQ0FBQ3JCLFFBQVAsRUFBZ0JSLFFBQWhCLHNCQUFnQkEsUUFBaEIsR0FBNkIsRUFBN0I7QUFFQSxXQUFPNkIsTUFBUDtBQUNIOztBQUVELFNBQU8sc0JBQVEsRUFBUixFQUFZM0IsU0FBWixDQUFQO0FBRUgiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgb3B0aW1hbCwgeyBvYmplY3QsIHN0cmluZywgYm9vbCwgYXJyYXksIHNoYXBlIH0gZnJvbSAnb3B0aW1hbCdcbmltcG9ydCB7IHBwYXRoLCBQb3J0YWJsZVBhdGgsIEZha2VGUyB9IGZyb20gJ0B5YXJucGtnL2ZzbGliJ1xuaW1wb3J0IHsgTm9kZVZNLCBWTVNjcmlwdCB9IGZyb20gJ3ZtMidcbmltcG9ydCBmYXVsdCBmcm9tICdmYXVsdCdcblxuXG5cbmNvbnN0IGJwX3BhZ2Vfc3BlYyA9IHtcbiAgICB1cmk6IHN0cmluZygpLnJlcXVpcmVkKCkubm90RW1wdHkoKSxcbiAgICBjb250ZW50czogc3RyaW5nKCkubm90RW1wdHkoKSxcbiAgICB0ZW1wbGF0ZTogc3RyaW5nKCkucmVxdWlyZWQoKS5ub3RFbXB0eSgpXG59XG5cbmNvbnN0IGJwX2Fzc2V0X3NwZWMgPSB7XG4gICAgdXJpOiBzdHJpbmcoKS5yZXF1aXJlZCgpLm5vdEVtcHR5KCksXG4gICAgc3JjOiBzdHJpbmcoKS5yZXF1aXJlZCgpLm5vdEVtcHR5KClcbn1cblxuY29uc3QgYnBfdmlld19zcGVjID0ge1xuICAgIHVyaTogc3RyaW5nKCkucmVxdWlyZWQoKS5ub3RFbXB0eSgpLFxuICAgIHNyYzogc3RyaW5nKCkucmVxdWlyZWQoKS5ub3RFbXB0eSgpLFxuICAgIGNhbm9uaWNhbDogYm9vbCgpXG59XG5cbmNvbnN0IGJwX2NvbGxlY3Rpb25fc3BlYyA9IHtcbiAgICBuYW1lOiBzdHJpbmcoKS5yZXF1aXJlZCgpLmNhbWVsQ2FzZSgpLm5vdEVtcHR5KCksXG4gICAgZ2xvYjogc3RyaW5nKCkucmVxdWlyZWQoKS5ub3RFbXB0eSgpLFxuICAgIHZpZXdzOiBhcnJheShzaGFwZShicF92aWV3X3NwZWMpKSxcbiAgICB4cmVmczogb2JqZWN0KClcbn1cblxuY29uc3QgYnBfc2V0dGluZ3Nfc3BlYyA9IHtcbiAgICBkaXJzOiBzaGFwZSh7XG4gICAgICAgIGJhc2U6IHN0cmluZygpLFxuICAgICAgICBvdXRwdXQ6IHN0cmluZygpLFxuICAgICAgICBzY3JpcHRzOiBzdHJpbmcoKSxcbiAgICAgICAgc3R5bGVzOiBzdHJpbmcoKVxuICAgIH0pLFxuICAgIG1hcmtkb3duOiBzaGFwZSh7XG4gICAgICAgIGNsYXNzZXM6IG9iamVjdCgpXG4gICAgfSlcbn1cblxuY29uc3QgYmx1ZXByaW50ID0ge1xuICAgIGNvbGxlY3Rpb25zOiBhcnJheShzaGFwZShicF9jb2xsZWN0aW9uX3NwZWMpKSxcbiAgICBwYWdlczogYXJyYXkoc2hhcGUoYnBfcGFnZV9zcGVjKSksXG4gICAgc2NyaXB0czogYXJyYXkoc2hhcGUoYnBfYXNzZXRfc3BlYykpLFxuICAgIHN0eWxlc2hlZXRzOiBhcnJheShzaGFwZShicF9hc3NldF9zcGVjKSksXG4gICAgYXNzZXRzOiBhcnJheShzaGFwZShicF9hc3NldF9zcGVjKSksXG4gICAgc2l0ZTogb2JqZWN0KCksXG4gICAgc2V0dGluZ3M6IHNoYXBlKGJwX3NldHRpbmdzX3NwZWMpXG59XG5cbmV4cG9ydCB0eXBlIENvbGxlY3Rpb25TcGVjID0ge1xuICAgIG5hbWU6IHN0cmluZyxcbiAgICBnbG9iOiBzdHJpbmcsXG4gICAgdmlld3M6IEFzc2V0U3BlY1tdXG59XG5cbmV4cG9ydCB0eXBlIFBhZ2VTcGVjID0ge1xuICAgIHVyaTogc3RyaW5nLFxuICAgIGNvbnRlbnRzPzogc3RyaW5nLFxuICAgIHRlbXBsYXRlOiBzdHJpbmdcbn1cblxuZXhwb3J0IHR5cGUgQXNzZXRTcGVjID0ge1xuICAgIHVyaTogc3RyaW5nLFxuICAgIHNyYzogc3RyaW5nXG59XG5cbmV4cG9ydCB0eXBlIFNldHRpbmdzU3BlYyA9IHtcbiAgICBkaXJzOiB7XG4gICAgICAgIGJhc2U6IHN0cmluZyxcbiAgICAgICAgb3V0cHV0OiBzdHJpbmcsXG4gICAgICAgIHNjcmlwdHM6IHN0cmluZyxcbiAgICAgICAgc3R5bGVzOiBzdHJpbmdcbiAgICB9XG59XG5cbmV4cG9ydCB0eXBlIENvbmZpZ3VyYXRpb24gPSB7XG4gICAgY29sbGVjdGlvbnM6IENvbGxlY3Rpb25TcGVjW10sXG4gICAgcGFnZXM6IFBhZ2VTcGVjW10sXG4gICAgc2NyaXB0czogQXNzZXRTcGVjW10sXG4gICAgc3R5bGVzaGVldHM6IEFzc2V0U3BlY1tdLFxuICAgIHNldHRpbmdzOiBTZXR0aW5nc1NwZWMsXG4gICAgc2l0ZTogb2JqZWN0XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBmaW5kQmFzZURpciAoZnM6IEZha2VGUzxQb3J0YWJsZVBhdGg+KTogUG9ydGFibGVQYXRoIHtcblxuICAgIGxldCBjdXJyZW50RGlyID0gcHJvY2Vzcy5jd2QoKVxuXG4gICAgd2hpbGUgKCFhd2FpdCBmcy5leGlzdHNQcm9taXNlKHBwYXRoLmpvaW4oY3VycmVudERpciwgJ3VtYW1pLmNvbmZpZy5qcycpKSkge1xuICAgICAgICBjb25zb2xlLmxvZyhgVGVzdGluZyBmb3IgYmFzZURpcjogJHtjdXJyZW50RGlyfWApXG4gICAgICAgIGlmIChjdXJyZW50RGlyID09ICcvJykgdGhyb3cgZmF1bHQoXCJDb3VsZCBub3QgZGV0ZXJtaW5lIGJhc2UgZGlyZWN0b3J5LiBObyB1bWFtaS5jb25maWcuanMgZmlsZSBmb3VuZC5cIik7XG4gICAgICAgIGN1cnJlbnREaXIgPSBwcGF0aC5kaXJuYW1lKGN1cnJlbnREaXIpXG4gICAgfVxuXG4gICAgcmV0dXJuIGN1cnJlbnREaXJcblxufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gbG9hZENvbmZpZ3VyYXRpb24gKGJhc2VEaXI6IFBvcnRhYmxlUGF0aCwgZnM6IEZha2VGUzxQb3J0YWJsZVBhdGg+KTogQ29uZmlndXJhdGlvbiB7XG5cbiAgICBjb25zdCBleGlzdHNKcyA9IGF3YWl0IGZzLmV4aXN0c1Byb21pc2UocHBhdGguam9pbihiYXNlRGlyLCAndW1hbWkuY29uZmlnLmpzJykpXG5cbiAgICBpZiAoZXhpc3RzSnMpIHtcbiAgICAgICAgY29uc3QgY29uZmlnU2NyaXB0U3JjID0gYXdhaXQgZnMucmVhZEZpbGVQcm9taXNlKHBwYXRoLmpvaW4oYmFzZURpciwgJ3VtYW1pLmNvbmZpZy5qcycpKVxuICAgICAgICBjb25zdCBjb25maWdTY3JpcHQgPSBuZXcgVk1TY3JpcHQoY29uZmlnU2NyaXB0U3JjKVxuICAgICAgICBjb25zdCB2bSA9IG5ldyBOb2RlVk0oKVxuXG4gICAgICAgIGNvbnN0IGxvYWRlZCA9IG9wdGltYWwodm0ucnVuKGNvbmZpZ1NjcmlwdCwgcHBhdGguam9pbihiYXNlRGlyLCAndW1hbWkuY29uZmlnLmpzJykpLCBibHVlcHJpbnQpXG4gICAgICAgIGxvYWRlZC5zZXR0aW5ncy5kaXJzLmJhc2UgfHw9IGJhc2VEaXJcbiAgICAgICAgbG9hZGVkLnNldHRpbmdzLmRpcnMub3V0cHV0IHx8PSBwcGF0aC5qb2luKGxvYWRlZC5zZXR0aW5ncy5kaXJzLmJhc2UsICdkaXN0JylcbiAgICAgICAgbG9hZGVkLnNldHRpbmdzLmRpcnMuc2NyaXB0cyB8fD0gcHBhdGguam9pbihsb2FkZWQuc2V0dGluZ3MuZGlycy5vdXRwdXQsICdzY3JpcHRzJylcbiAgICAgICAgbG9hZGVkLnNldHRpbmdzLmRpcnMuc3R5bGVzIHx8PSBwcGF0aC5qb2luKGxvYWRlZC5zZXR0aW5ncy5kaXJzLm91dHB1dCwgJ3N0eWxlcycpXG4gICAgICAgIGxvYWRlZC5zZXR0aW5ncy5kaXJzLmFzc2V0cyB8fD0gcHBhdGguam9pbihsb2FkZWQuc2V0dGluZ3MuZGlycy5vdXRwdXQsICdhc3NldHMnKVxuXG4gICAgICAgIGxvYWRlZC5zZXR0aW5ncy5tYXJrZG93biB8fD0ge31cblxuICAgICAgICByZXR1cm4gbG9hZGVkXG4gICAgfVxuXG4gICAgcmV0dXJuIG9wdGltYWwoe30sIGJsdWVwcmludClcblxufVxuXG4iXX0=