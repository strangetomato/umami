"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLogger = createLogger;

var _pino = _interopRequireDefault(require("pino"));

var _pinoPretty = _interopRequireDefault(require("pino-pretty"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createLogger() {
  return (0, _pino.default)({
    prettyPrint: {
      levelFirst: true
    },
    prettifier: _pinoPretty.default
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlsL0xvZy50cyJdLCJuYW1lcyI6WyJjcmVhdGVMb2dnZXIiLCJwcmV0dHlQcmludCIsImxldmVsRmlyc3QiLCJwcmV0dGlmaWVyIiwicGlub1ByZXR0eSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7O0FBRU8sU0FBU0EsWUFBVCxHQUF3QjtBQUMzQixTQUFPLG1CQUFLO0FBQUVDLElBQUFBLFdBQVcsRUFBRTtBQUFFQyxNQUFBQSxVQUFVLEVBQUU7QUFBZCxLQUFmO0FBQXFDQyxJQUFBQSxVQUFVLEVBQUVDO0FBQWpELEdBQUwsQ0FBUDtBQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHBpbm8gZnJvbSAncGlubydcbmltcG9ydCBwaW5vUHJldHR5IGZyb20gJ3Bpbm8tcHJldHR5J1xuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlTG9nZ2VyKCkge1xuICAgIHJldHVybiBwaW5vKHsgcHJldHR5UHJpbnQ6IHsgbGV2ZWxGaXJzdDogdHJ1ZSB9LCBwcmV0dGlmaWVyOiBwaW5vUHJldHR5IH0pXG59Il19