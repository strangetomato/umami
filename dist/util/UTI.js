"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UTIController = void 0;
const types = [{
  name: "public.item"
}, {
  name: "public.content"
}, {
  name: "public.message"
}, {
  name: "public.contact"
}, {
  name: "public.composite-content",
  conformsTo: "public.content"
}, {
  name: "public.data",
  conformsTo: "public.item"
}, {
  name: "public.database"
}, {
  name: "public.archive",
  conformsTo: "public.data"
}, {
  name: "public.directory",
  conformsTo: "public.item"
}, {
  name: "public.executable",
  conformsTo: "public.item"
}, {
  name: "com.microsoft.windows-dynamic-link-library",
  conformsTo: ["public.executable", "public.data"],
  mimeType: "application/x-msdownload",
  fileNameExtension: ".dll"
}, {
  name: "com.microsoft.windows-executable",
  conformsTo: ["public.executable", "public.data"],
  mimeType: "application/x-msdownload",
  fileNameExtension: ".exe"
}, {
  name: "public.object-code",
  conformsTo: ["public.executable", "public.data"],
  fileNameExtension: ".o"
}, {
  name: "public.unix-executable",
  conformsTo: ["public.executable", "public.data"]
}, {
  name: "com.sun.java-class",
  conformsTo: ["public.executable", "public.data"],
  fileNameExtension: ".class"
}, {
  name: "public.key-value",
  conformsTo: ["public.data"]
}, {
  name: "com.sun.java-properties",
  conformsTo: ["public.key-value"],
  fileNameExtension: ".properties",
  mimeType: "text/x-java-properties"
}, {
  name: "public.zip-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".zip",
  mimeType: "application/zip"
}, {
  name: "com.sun.java-keystore",
  conformsTo: "public.archive",
  fileNameExtension: ".jks"
}, {
  name: "com.sun.java-archive",
  conformsTo: ["public.executable", "public.zip-archive"],
  mimeType: "application/x-java-archive",
  fileNameExtension: ".jar"
}, {
  name: "com.sun.java-web-archive",
  conformsTo: ["public.executable", "public.zip-archive"],
  fileNameExtension: ".war"
}, {
  name: "com.sun.java-enterprise-archive",
  conformsTo: ["public.executable", "public.zip-archive"],
  fileNameExtension: ".ear"
}, {
  name: "public.folder",
  conformsTo: "public.directory"
}, {
  name: "public.volume",
  conformsTo: "public.folder"
}, {
  name: "com.apple.resolvable"
}, {
  name: "public.symlink",
  conformsTo: ["public.item", "com.apple.resolvable"]
}, {
  name: "public.url",
  conformsTo: "public.data"
}, {
  name: "public.file-url",
  conformsTo: "public.url"
}, {
  name: "public.text",
  conformsTo: ["public.data", "public.content"]
}, {
  name: "public.plain-text",
  conformsTo: "public.text",
  fileNameExtension: ".txt",
  mimeType: "text/plain"
}, {
  name: "public.utf8-plain-text",
  conformsTo: "public.plain-text"
}, {
  name: "public.utf16-external-plain-​text",
  conformsTo: "public.plain-text"
}, {
  name: "public.utf16-plain-text",
  conformsTo: "public.plain-text"
}, {
  name: "public.rtf",
  conformsTo: "public.text",
  fileNameExtension: ".rtf",
  mimeType: "text/rtf"
}, {
  name: "public.html",
  conformsTo: "public.text",
  fileNameExtension: [".html", ".htm"],
  mimeType: "text/html"
}, {
  name: "public.xml",
  conformsTo: "public.text",
  fileNameExtension: ".xml",
  mimeType: "text/xml"
}, {
  name: "public.json",
  conformsTo: "public.text",
  fileNameExtension: ".json",
  mimeType: ["application/json", "text/json"]
}, {
  name: "public.toml",
  conformsTo: "public.text",
  fileNameExtension: ".toml",
  mimeType: "application/toml"
}, {
  name: "public.ini",
  conformsTo: "public.text",
  fileNameExtension: ".ini",
  mimeType: "zz-application/zz-winassoc-ini"
}, {
  name: "public.source-code",
  conformsTo: "public.plain-text"
}, {
  name: "public.script",
  conformsTo: "public.source-code"
}, {
  name: "com.sun.java-source",
  conformsTo: "public.source-code",
  fileNameExtension: [".java", ".jav"]
}, {
  name: "public.shell-script",
  conformsTo: "public.script",
  fileNameExtension: [".sh", ".command"],
  mimeType: ["text/x-shellscript"]
}, {
  name: "public.bash-script",
  conformsTo: "public.shell-script",
  fileNameExtension: [".bash"]
}, {
  name: "public.ksh-script",
  conformsTo: "public.shell-script",
  fileNameExtension: [".ksh"],
  mimeType: ["text/x-script.ksh", "application/x-ksh"]
}, {
  name: "public.assembly-source",
  conformsTo: "public.source-code",
  fileNameExtension: ".s"
}, {
  name: "com.netscape.javascript-source",
  conformsTo: ["public.source-code", "public.executable"],
  fileNameExtension: [".js", ".jscript", ".javascript"],
  mimeType: ["application/javascript", "application/ecmascript"]
}, {
  name: "public.bzip2-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".bzip2"
}, {
  name: "org.gnu.gnu-tar-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".tar",
  mimeType: "application/x-tar"
}, {
  name: "public.tar-archive",
  conformsTo: "org.gnu.gnu-tar-archive",
  fileNameExtension: ".tar",
  mimeType: ["application/x-tar", "application/tar"]
}, {
  name: "org.gnu.gnu-zip-archive",
  conformsTo: "public.archive",
  fileNameExtension: [".gz", ".gzip"],
  mimeType: ["application/gzip", "application/x-gzip"]
}, {
  name: "org.gnu.gnu-zip-tar-archive",
  conformsTo: "org.gnu.gnu-zip-archive",
  mimeType: "application/tar+gzip",
  fileNameExtension: [".tar.gz", ".tgz"]
}, {
  name: "com.apple.xar-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".xar"
}, {
  name: "public.xz-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".xz",
  mimeType: "application/x-xz"
}, {
  name: "public.7z-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".7z",
  mimeType: "application/x-7z-compressed"
}, {
  name: "public.rar-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".rar",
  mimeType: ["application/x-rar-compressed", "application/vnd.rar"]
}, {
  name: "public.cpio-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".cpio",
  mimeType: "application/x-cpio"
}, {
  name: "public.rpm-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".rpm",
  mimeType: "application/x-rpm"
}, {
  name: "public.ar-archive",
  conformsTo: "public.archive",
  fileNameExtension: [".a", ".lib"]
}, {
  name: "public.deb-archive",
  conformsTo: "public.ar-archive",
  fileNameExtension: ".deb",
  mimeType: "application/vnd.debian.binary-package"
}, {
  name: "public.arch-linux-archive",
  conformsTo: "public.archive",
  fileNameExtension: ".pkg.tar.xz"
}, {
  name: "com.apple.package",
  conformsTo: "public.directory"
}, {
  name: "com.apple.application",
  conformsTo: "public.data"
}, {
  name: "com.apple.application-bundle",
  conformsTo: "com.apple.application",
  fileNameExtension: [".app", ".framework", ".kext", ".plugin", ".docset", ".xpc", ".qlgenerator", ".component", ".saver", ".mdimport"]
}, {
  name: "public.disk-image",
  conformsTo: "public.archive"
}, {
  name: "public.image",
  conformsTo: ["public.data", "public.content"]
}, {
  name: "public.case-insensitive-text"
}, {
  name: "public.filename-extension",
  conformsTo: "public.case-insensitive-text"
}, {
  name: "public.mime-type",
  conformsTo: "public.case-insensitive-text"
}, {
  name: "public.presentation",
  conformsTo: "public.composite-content"
}, {
  name: "com.microsoft.word.doc",
  conformsTo: ["public.data", "public.composite-content"],
  fileNameExtension: ".docx"
}, {
  name: "com.microsoft.excel.xls",
  conformsTo: ["public.data", "public.composite-content"],
  fileNameExtension: ".xlsx"
}, {
  name: "com.microsoft.powerpoint.ppt",
  conformsTo: ["public.data", "public.presentation"],
  fileNameExtension: ".ppt"
}, {
  name: "com.microsoft.word.wordml",
  conformsTo: ["public.data", "public.composite-content"]
}, {
  name: "public.image",
  conformsTo: ["public.data", "public.content"]
}, {
  name: "public.jpeg",
  conformsTo: "public.image",
  fileNameExtension: [".jpg", ".jpeg"]
}, {
  name: "com.adobe.postscript",
  conformsTo: "public.image",
  fileNameExtension: [".ps"],
  mimeType: ["application/postscript"]
}, {
  name: "net.daringfireball.markdown",
  conformsTo: "public.text",
  fileNameExtension: [".md", ".markdown"],
  mimeType: ["text/markdown"]
}, {
  name: "com.apple.disk-image",
  conformsTo: "public.disk-image",
  fileNameExtension: [".dmg", ".smi", ".img"],
  mimeType: ["application/x-apple-diskimage"]
}, {
  name: "public.security.private-key",
  conformsTo: "public.data"
}, {
  name: "public.security.certificate",
  conformsTo: "public.data"
}, {
  name: "public.security.certificate.trust",
  conformsTo: "public.security.certificate"
}, {
  name: "public.security.certificate.request",
  conformsTo: "public.data"
}, {
  name: "public.pkcs8",
  conformsTo: "public.data",
  fileNameExtension: [".p8", ".key"],
  mimeType: "application/pkcs8"
}, {
  name: "public.pkcs10",
  conformsTo: "public.data",
  fileNameExtension: [".p10", ".csr"],
  mimeType: "application/pkcs10"
}, {
  name: "public.pkix-cert",
  conformsTo: "public.security.certificate",
  fileNameExtension: ".cer",
  mimeType: "application/pkix-cert"
}, {
  name: "public.pkix-crl",
  conformsTo: "public.data",
  fileNameExtension: ".crl",
  mimeType: "application/pkix-crl"
}, {
  name: "public.pkcs7-mime",
  conformsTo: "public.data",
  fileNameExtension: ".p7c",
  mimeType: "application/pkcs7-mime"
}, {
  name: "public.x-x509-ca-cert",
  conformsTo: "public.security.certificate",
  fileNameExtension: [".crt", ".der"],
  mimeType: "application/x-x509-ca-cert"
}, {
  name: "public.x-x509-user-cert",
  conformsTo: "public.security.certificate",
  fileNameExtension: ".crt",
  mimeType: "application/x-x509-user-cert"
}, {
  name: "public.x-pkcs7-crl",
  conformsTo: "public.data",
  fileNameExtension: ".crl",
  mimeType: "application/x-pkcs7-crl"
}, {
  name: "public.x-pkcs12",
  conformsTo: "public.data",
  fileNameExtension: ".pem",
  mimeType: "application/x-pem-file"
}, {
  name: "public.x-pem-file",
  conformsTo: "public.data",
  fileNameExtension: [".p12", ".pfx"],
  mimeType: "application/x-pkcs12"
}, {
  name: "public.x-pkcs7-certificates",
  conformsTo: "public.security.certificate",
  fileNameExtension: [".p7b", ".spc"],
  mimeType: "application/x-pkcs7-certificates"
}, {
  name: "public.x-pkcs7-certreqresp",
  conformsTo: "public.data",
  fileNameExtension: ".p7r",
  mimeType: "application/x-pkcs7-certreqresp"
}];
/**
 * Object representing a UTI
 * @param {string} name
 * @param {string} conforms
 *
 * @property {string} name
 * @property {string} conforms
 */

class UTI {
  constructor(name, conforms) {
    Object.defineProperties(this, {
      name: {
        value: name
      },
      conforms: {
        value: conforms
      }
    });
  }
  /**
   * Check for conformity
   * @param {UTI} other
   * @return {boolean} true if other conforms to the receiver
   */


  conformsTo(other) {
    if (this === other || this.conforms.has(other)) {
      return true;
    }

    for (const u of this.conforms) {
      if (u.conformsTo(other)) {
        return true;
      }
    }

    return false;
  }

  toString() {
    return this.name;
  }
  /**
   * Deliver JSON representation of the UTI.
   * Sample result
   * ´´´json
   * {
   *   "name": "myUTI",
   *   "conformsTo": [ "uti1", "uti2"]
   * }
   * ´´´
   * @return {Object} json representation of the UTI
   */


  toJSON() {
    return {
      name: this.name,
      conforms: this.conforms
    };
  }

}
/**
 * Registry of UTIs
 * @property {Map<string,UTI>} registry
 * @property {Map<string,UTI>} utiByMimeType
 * @property {Map<string,UTI>} utiByFileNameExtension
 */


class UTIController {
  constructor() {
    Object.defineProperties(this, {
      registry: {
        value: new Map()
      },
      utiByMimeType: {
        value: new Map()
      },
      utiByFileNameExtension: {
        value: new Map()
      }
    });
    this.register(types);
  }
  /**
   * registers additional types
   * @param {Object[]} types
   */


  register(types) {
    for (const u of types) {
      if (u.fileNameExtension !== undefined) {
        this.assignExtensions(u.name, Array.isArray(u.fileNameExtension) ? u.fileNameExtension : [u.fileNameExtension]);
      }

      if (u.mimeType !== undefined) {
        this.assignMimeTypes(u.name, Array.isArray(u.mimeType) ? u.mimeType : [u.mimeType]);
      }

      const conforms = new Set();

      if (u.conformsTo !== undefined) {
        const ct = Array.isArray(u.conformsTo) ? u.conformsTo : [u.conformsTo];
        ct.forEach(name => {
          const aUTI = this.getUTI(name);

          if (aUTI === undefined) {
            throw new Error(`Referenced UTI not known: ${name}`);
          } else {
            conforms.add(aUTI);
          }
        });
      }

      const nu = new UTI(u.name, conforms);
      this.registry.set(nu.name, nu);
    }
  }
  /**
   * Lookup a given UTI.
   * @param {string} name UTI
   * @return {string} UTI for the given name or undefined if UTI is not present.
   */


  getUTI(name) {
    return this.registry.get(name);
  }
  /**
   * Lookup a UTIs for a mime type.
   * @param {string} mimeType mime type to get UTIs for
   * @return {string} UTI for the given mime type or undefined if no UTI is registerd for the mime type
   */


  getUTIsForMimeType(mimeType) {
    return this.utiByMimeType.get(mimeType);
  }
  /**
   * Lookup a UTI for a file name.
   * First the file name extension is extracted.
   * Then a lookup in the reistered UTIs for file name extions is executed.
   * @param {string} fileName file to detect UTI for
   * @return {string} UTI for the given fileName or undefined if no UTI is registerd for the file names extension
   */


  getUTIsForFileName(fileName) {
    const m = fileName.match(/(\.[a-zA-Z_0-9]+)$/);
    return m ? this.utiByFileNameExtension.get(m[1]) : undefined;
  }
  /**
   * Check whenever two UTI are conformant.
   * If a conforms to b and b conforms to c then a also conforms to c.
   * @param {string} a first UTI
   * @param {string} b second UTI
   * @return {boolean} true if UTI a conforms to UTI b.
   */


  conformsTo(a, b) {
    const ua = this.registry.get(a);
    return ua === undefined ? false : ua.conformsTo(this.registry.get(b));
  }
  /**
   * Lookup a UTI for a file name and check conformance
   * @param {string} fileName file to detect UTI for
   * @param {string} uti to check conformance egainst
   * @return {boolean} ture if utils for file name are conformant
   */


  fileNameConformsTo(fileName, uti) {
    const utis = this.getUTIsForFileName(fileName);

    if (utis === undefined) {
      return false;
    }

    for (const u of utis) {
      if (this.conformsTo(u, uti)) {
        return true;
      }
    }

    return false;
  }

  assignMimeTypes(name, mimTypes) {
    mimTypes.forEach(type => {
      const u = this.utiByMimeType.get(type);

      if (u === undefined) {
        this.utiByMimeType.set(type, [name]);
      } else {
        u.push(name);
      }
    });
  }

  assignExtensions(name, extensions) {
    extensions.forEach(ext => {
      const e = this.utiByFileNameExtension.get(ext);

      if (e === undefined) {
        this.utiByFileNameExtension.set(ext, [name]);
      } else {
        e.push(name);
      }
    });
  }

}

exports.UTIController = UTIController;
const defaultController = new UTIController();
defaultController.register([{
  name: 'public.svg-image',
  conformsTo: ['public.image', 'public.xml'],
  mimeType: 'application/svg+xml',
  fileNameExtension: '.svg'
}, {
  name: 'com.micrososft.typescript',
  conformsTo: ['public.source-code'],
  fileNameExtension: '.ts'
}, {
  name: 'org.yaml',
  conformsTo: ['public.text'],
  fileNameExtension: ['.yml', '.yaml'],
  mimeType: ['application/yaml', 'text/yaml']
}]);
var _default = defaultController;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlsL1VUSS50cyJdLCJuYW1lcyI6WyJ0eXBlcyIsIm5hbWUiLCJjb25mb3Jtc1RvIiwibWltZVR5cGUiLCJmaWxlTmFtZUV4dGVuc2lvbiIsIlVUSSIsImNvbnN0cnVjdG9yIiwiY29uZm9ybXMiLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0aWVzIiwidmFsdWUiLCJvdGhlciIsImhhcyIsInUiLCJ0b1N0cmluZyIsInRvSlNPTiIsIlVUSUNvbnRyb2xsZXIiLCJyZWdpc3RyeSIsIk1hcCIsInV0aUJ5TWltZVR5cGUiLCJ1dGlCeUZpbGVOYW1lRXh0ZW5zaW9uIiwicmVnaXN0ZXIiLCJ1bmRlZmluZWQiLCJhc3NpZ25FeHRlbnNpb25zIiwiQXJyYXkiLCJpc0FycmF5IiwiYXNzaWduTWltZVR5cGVzIiwiU2V0IiwiY3QiLCJmb3JFYWNoIiwiYVVUSSIsImdldFVUSSIsIkVycm9yIiwiYWRkIiwibnUiLCJzZXQiLCJnZXQiLCJnZXRVVElzRm9yTWltZVR5cGUiLCJnZXRVVElzRm9yRmlsZU5hbWUiLCJmaWxlTmFtZSIsIm0iLCJtYXRjaCIsImEiLCJiIiwidWEiLCJmaWxlTmFtZUNvbmZvcm1zVG8iLCJ1dGkiLCJ1dGlzIiwibWltVHlwZXMiLCJ0eXBlIiwicHVzaCIsImV4dGVuc2lvbnMiLCJleHQiLCJlIiwiZGVmYXVsdENvbnRyb2xsZXIiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE1BQU1BLEtBQUssR0FBRyxDQUNaO0FBQ0VDLEVBQUFBLElBQUksRUFBRTtBQURSLENBRFksRUFJWjtBQUNFQSxFQUFBQSxJQUFJLEVBQUU7QUFEUixDQUpZLEVBT1o7QUFDRUEsRUFBQUEsSUFBSSxFQUFFO0FBRFIsQ0FQWSxFQVVaO0FBQ0VBLEVBQUFBLElBQUksRUFBRTtBQURSLENBVlksRUFhWjtBQUNFQSxFQUFBQSxJQUFJLEVBQUUsMEJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0FiWSxFQWlCWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsYUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQWpCWSxFQXFCWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUU7QUFEUixDQXJCWSxFQXdCWjtBQUNFQSxFQUFBQSxJQUFJLEVBQUUsZ0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0F4QlksRUE0Qlo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLGtCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRTtBQUZkLENBNUJZLEVBZ0NaO0FBQ0VELEVBQUFBLElBQUksRUFBRSxtQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQWhDWSxFQW9DWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsNENBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLENBQUMsbUJBQUQsRUFBc0IsYUFBdEIsQ0FGZDtBQUdFQyxFQUFBQSxRQUFRLEVBQUUsMEJBSFo7QUFJRUMsRUFBQUEsaUJBQWlCLEVBQUU7QUFKckIsQ0FwQ1ksRUEwQ1o7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLGtDQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxDQUFDLG1CQUFELEVBQXNCLGFBQXRCLENBRmQ7QUFHRUMsRUFBQUEsUUFBUSxFQUFFLDBCQUhaO0FBSUVDLEVBQUFBLGlCQUFpQixFQUFFO0FBSnJCLENBMUNZLEVBZ0RaO0FBQ0VILEVBQUFBLElBQUksRUFBRSxvQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixhQUF0QixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBaERZLEVBcURaO0FBQ0VILEVBQUFBLElBQUksRUFBRSx3QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixhQUF0QjtBQUZkLENBckRZLEVBeURaO0FBQ0VELEVBQUFBLElBQUksRUFBRSxvQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixhQUF0QixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBekRZLEVBOERaO0FBQ0VILEVBQUFBLElBQUksRUFBRSxrQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFEO0FBRmQsQ0E5RFksRUFrRVo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLHlCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxDQUFDLGtCQUFELENBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsYUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0FsRVksRUF3RVo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLG9CQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxnQkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQXhFWSxFQThFWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsdUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGdCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBOUVZLEVBbUZaO0FBQ0VILEVBQUFBLElBQUksRUFBRSxzQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixvQkFBdEIsQ0FGZDtBQUdFQyxFQUFBQSxRQUFRLEVBQUUsNEJBSFo7QUFJRUMsRUFBQUEsaUJBQWlCLEVBQUU7QUFKckIsQ0FuRlksRUF5Rlo7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLDBCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxDQUFDLG1CQUFELEVBQXNCLG9CQUF0QixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBekZZLEVBOEZaO0FBQ0VILEVBQUFBLElBQUksRUFBRSxpQ0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixvQkFBdEIsQ0FGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRTtBQUhyQixDQTlGWSxFQW1HWjtBQUNFSCxFQUFBQSxJQUFJLEVBQUUsZUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQW5HWSxFQXVHWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsZUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXZHWSxFQTJHWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUU7QUFEUixDQTNHWSxFQThHWjtBQUNFQSxFQUFBQSxJQUFJLEVBQUUsZ0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLENBQUMsYUFBRCxFQUFnQixzQkFBaEI7QUFGZCxDQTlHWSxFQWtIWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsWUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQWxIWSxFQXNIWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsaUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0F0SFksRUEwSFo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLGFBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLENBQUMsYUFBRCxFQUFnQixnQkFBaEI7QUFGZCxDQTFIWSxFQThIWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsbUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGFBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsTUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0E5SFksRUFvSVo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLHdCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRTtBQUZkLENBcElZLEVBd0laO0FBQ0VELEVBQUFBLElBQUksRUFBRSxtQ0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXhJWSxFQTRJWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUseUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0E1SVksRUFnSlo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLFlBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGFBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsTUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0FoSlksRUFzSlo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLGFBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGFBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxPQUFELEVBQVUsTUFBVixDQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQXRKWSxFQTRKWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsWUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsYUFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQTVKWSxFQWtLWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsYUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsYUFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxPQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUUsQ0FBQyxrQkFBRCxFQUFxQixXQUFyQjtBQUpaLENBbEtZLEVBd0taO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxhQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE9BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBeEtZLEVBOEtaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxZQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBOUtZLEVBb0xaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxvQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXBMWSxFQXdMWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsZUFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXhMWSxFQTRMWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUscUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLG9CQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsT0FBRCxFQUFVLE1BQVY7QUFIckIsQ0E1TFksRUFpTVo7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLHFCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxlQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsS0FBRCxFQUFRLFVBQVIsQ0FIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFLENBQUMsb0JBQUQ7QUFKWixDQWpNWSxFQXVNWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsb0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLHFCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsT0FBRDtBQUhyQixDQXZNWSxFQTRNWjtBQUNFSCxFQUFBQSxJQUFJLEVBQUUsbUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLHFCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsTUFBRCxDQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUUsQ0FBQyxtQkFBRCxFQUFzQixtQkFBdEI7QUFKWixDQTVNWSxFQWtOWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsd0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLG9CQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBbE5ZLEVBdU5aO0FBQ0VILEVBQUFBLElBQUksRUFBRSxnQ0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxvQkFBRCxFQUF1QixtQkFBdkIsQ0FGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxDQUFDLEtBQUQsRUFBUSxVQUFSLEVBQW9CLGFBQXBCLENBSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRSxDQUFDLHdCQUFELEVBQTJCLHdCQUEzQjtBQUpaLENBdk5ZLEVBNk5aO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxzQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUU7QUFIckIsQ0E3TlksRUFrT1o7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLHlCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxnQkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQWxPWSxFQXdPWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsb0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLHlCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRSxDQUFDLG1CQUFELEVBQXNCLGlCQUF0QjtBQUpaLENBeE9ZLEVBOE9aO0FBQ0VGLEVBQUFBLElBQUksRUFBRSx5QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxLQUFELEVBQVEsT0FBUixDQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUUsQ0FBQyxrQkFBRCxFQUFxQixvQkFBckI7QUFKWixDQTlPWSxFQW9QWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsNkJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLHlCQUZkO0FBR0VDLEVBQUFBLFFBQVEsRUFBRSxzQkFIWjtBQUlFQyxFQUFBQSxpQkFBaUIsRUFBRSxDQUFDLFNBQUQsRUFBWSxNQUFaO0FBSnJCLENBcFBZLEVBMFBaO0FBQ0VILEVBQUFBLElBQUksRUFBRSx1QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUU7QUFIckIsQ0ExUFksRUErUFo7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLG1CQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxnQkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxLQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQS9QWSxFQXFRWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsbUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGdCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLEtBSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBclFZLEVBMlFaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxvQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsTUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFLENBQUMsOEJBQUQsRUFBaUMscUJBQWpDO0FBSlosQ0EzUVksRUFpUlo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLHFCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxnQkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxPQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQWpSWSxFQXVSWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsb0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGdCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBdlJZLEVBNlJaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxtQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxJQUFELEVBQU0sTUFBTjtBQUhyQixDQTdSWSxFQWtTWjtBQUNFSCxFQUFBQSxJQUFJLEVBQUUsb0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLG1CQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBbFNZLEVBd1NaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSwyQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsZ0JBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUU7QUFIckIsQ0F4U1ksRUE2U1o7QUFDRUgsRUFBQUEsSUFBSSxFQUFFLG1CQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRTtBQUZkLENBN1NZLEVBaVRaO0FBQ0VELEVBQUFBLElBQUksRUFBRSx1QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQWpUWSxFQXFUWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsOEJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLHVCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQ2pCLE1BRGlCLEVBRWpCLFlBRmlCLEVBR2pCLE9BSGlCLEVBSWpCLFNBSmlCLEVBS2pCLFNBTGlCLEVBTWpCLE1BTmlCLEVBT2pCLGNBUGlCLEVBUWpCLFlBUmlCLEVBU2pCLFFBVGlCLEVBVWpCLFdBVmlCO0FBSHJCLENBclRZLEVBcVVaO0FBQ0VILEVBQUFBLElBQUksRUFBRSxtQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXJVWSxFQXlVWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsY0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELEVBQWdCLGdCQUFoQjtBQUZkLENBelVZLEVBNlVaO0FBQ0VELEVBQUFBLElBQUksRUFBRTtBQURSLENBN1VZLEVBZ1ZaO0FBQ0VBLEVBQUFBLElBQUksRUFBRSwyQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQWhWWSxFQW9WWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsa0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0FwVlksRUF3Vlo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLHFCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRTtBQUZkLENBeFZZLEVBNFZaO0FBQ0VELEVBQUFBLElBQUksRUFBRSx3QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELEVBQWdCLDBCQUFoQixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBNVZZLEVBaVdaO0FBQ0VILEVBQUFBLElBQUksRUFBRSx5QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELEVBQWdCLDBCQUFoQixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBaldZLEVBc1daO0FBQ0VILEVBQUFBLElBQUksRUFBRSw4QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELEVBQWdCLHFCQUFoQixDQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFO0FBSHJCLENBdFdZLEVBMldaO0FBQ0VILEVBQUFBLElBQUksRUFBRSwyQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELEVBQWdCLDBCQUFoQjtBQUZkLENBM1dZLEVBK1daO0FBQ0VELEVBQUFBLElBQUksRUFBRSxjQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxDQUFDLGFBQUQsRUFBZ0IsZ0JBQWhCO0FBRmQsQ0EvV1ksRUFtWFo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLGFBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGNBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxNQUFELEVBQVMsT0FBVDtBQUhyQixDQW5YWSxFQXdYWjtBQUNFSCxFQUFBQSxJQUFJLEVBQUUsc0JBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGNBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxLQUFELENBSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRSxDQUFDLHdCQUFEO0FBSlosQ0F4WFksRUE4WFo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLDZCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsS0FBRCxFQUFRLFdBQVIsQ0FIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFLENBQUMsZUFBRDtBQUpaLENBOVhZLEVBb1laO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxzQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsbUJBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxNQUFELEVBQVMsTUFBVCxFQUFpQixNQUFqQixDQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUUsQ0FBQywrQkFBRDtBQUpaLENBcFlZLEVBMFlaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSw2QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQTFZWSxFQThZWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsNkJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFO0FBRmQsQ0E5WVksRUFrWlo7QUFDRUQsRUFBQUEsSUFBSSxFQUFFLG1DQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRTtBQUZkLENBbFpZLEVBc1paO0FBQ0VELEVBQUFBLElBQUksRUFBRSxxQ0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUU7QUFGZCxDQXRaWSxFQTBaWjtBQUNFRCxFQUFBQSxJQUFJLEVBQUUsY0FEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsYUFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxDQUFDLEtBQUQsRUFBUSxNQUFSLENBSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBMVpZLEVBZ2FaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxlQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsTUFBRCxFQUFTLE1BQVQsQ0FIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0FoYVksRUFzYVo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLGtCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSw2QkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQXRhWSxFQTRhWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsaUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGFBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsTUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0E1YVksRUFrYlo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLG1CQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBbGJZLEVBd2JaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSx1QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsNkJBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsQ0FBQyxNQUFELEVBQVMsTUFBVCxDQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQXhiWSxFQThiWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUseUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLDZCQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLE1BSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBOWJZLEVBb2NaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSxvQkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsYUFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQXBjWSxFQTBjWjtBQUNFRixFQUFBQSxJQUFJLEVBQUUsaUJBRFI7QUFFRUMsRUFBQUEsVUFBVSxFQUFFLGFBRmQ7QUFHRUUsRUFBQUEsaUJBQWlCLEVBQUUsTUFIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0ExY1ksRUFnZFo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLG1CQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSxhQUZkO0FBR0VFLEVBQUFBLGlCQUFpQixFQUFFLENBQUMsTUFBRCxFQUFTLE1BQVQsQ0FIckI7QUFJRUQsRUFBQUEsUUFBUSxFQUFFO0FBSlosQ0FoZFksRUFzZFo7QUFDRUYsRUFBQUEsSUFBSSxFQUFFLDZCQURSO0FBRUVDLEVBQUFBLFVBQVUsRUFBRSw2QkFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxDQUFDLE1BQUQsRUFBUyxNQUFULENBSHJCO0FBSUVELEVBQUFBLFFBQVEsRUFBRTtBQUpaLENBdGRZLEVBNGRaO0FBQ0VGLEVBQUFBLElBQUksRUFBRSw0QkFEUjtBQUVFQyxFQUFBQSxVQUFVLEVBQUUsYUFGZDtBQUdFRSxFQUFBQSxpQkFBaUIsRUFBRSxNQUhyQjtBQUlFRCxFQUFBQSxRQUFRLEVBQUU7QUFKWixDQTVkWSxDQUFkO0FBb2VBOzs7Ozs7Ozs7QUFRQSxNQUFNRSxHQUFOLENBQVU7QUFDUkMsRUFBQUEsV0FBVyxDQUFDTCxJQUFELEVBQU9NLFFBQVAsRUFBaUI7QUFDMUJDLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsSUFBeEIsRUFBOEI7QUFDNUJSLE1BQUFBLElBQUksRUFBRTtBQUNKUyxRQUFBQSxLQUFLLEVBQUVUO0FBREgsT0FEc0I7QUFJNUJNLE1BQUFBLFFBQVEsRUFBRTtBQUNSRyxRQUFBQSxLQUFLLEVBQUVIO0FBREM7QUFKa0IsS0FBOUI7QUFRRDtBQUVEOzs7Ozs7O0FBS0FMLEVBQUFBLFVBQVUsQ0FBQ1MsS0FBRCxFQUFRO0FBQ2hCLFFBQUksU0FBU0EsS0FBVCxJQUFrQixLQUFLSixRQUFMLENBQWNLLEdBQWQsQ0FBa0JELEtBQWxCLENBQXRCLEVBQWdEO0FBQzlDLGFBQU8sSUFBUDtBQUNEOztBQUVELFNBQUssTUFBTUUsQ0FBWCxJQUFnQixLQUFLTixRQUFyQixFQUErQjtBQUM3QixVQUFJTSxDQUFDLENBQUNYLFVBQUYsQ0FBYVMsS0FBYixDQUFKLEVBQXlCO0FBQ3ZCLGVBQU8sSUFBUDtBQUNEO0FBQ0Y7O0FBQ0QsV0FBTyxLQUFQO0FBQ0Q7O0FBRURHLEVBQUFBLFFBQVEsR0FBRztBQUNULFdBQU8sS0FBS2IsSUFBWjtBQUNEO0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFXQWMsRUFBQUEsTUFBTSxHQUFHO0FBQ1AsV0FBTztBQUNMZCxNQUFBQSxJQUFJLEVBQUUsS0FBS0EsSUFETjtBQUVMTSxNQUFBQSxRQUFRLEVBQUUsS0FBS0E7QUFGVixLQUFQO0FBSUQ7O0FBbERPO0FBcURWOzs7Ozs7OztBQU1PLE1BQU1TLGFBQU4sQ0FBb0I7QUFDekJWLEVBQUFBLFdBQVcsR0FBRztBQUNaRSxJQUFBQSxNQUFNLENBQUNDLGdCQUFQLENBQXdCLElBQXhCLEVBQThCO0FBQzVCUSxNQUFBQSxRQUFRLEVBQUU7QUFDUlAsUUFBQUEsS0FBSyxFQUFFLElBQUlRLEdBQUo7QUFEQyxPQURrQjtBQUk1QkMsTUFBQUEsYUFBYSxFQUFFO0FBQ2JULFFBQUFBLEtBQUssRUFBRSxJQUFJUSxHQUFKO0FBRE0sT0FKYTtBQU81QkUsTUFBQUEsc0JBQXNCLEVBQUU7QUFDdEJWLFFBQUFBLEtBQUssRUFBRSxJQUFJUSxHQUFKO0FBRGU7QUFQSSxLQUE5QjtBQVlBLFNBQUtHLFFBQUwsQ0FBY3JCLEtBQWQ7QUFDRDtBQUVEOzs7Ozs7QUFJQXFCLEVBQUFBLFFBQVEsQ0FBQ3JCLEtBQUQsRUFBUTtBQUNkLFNBQUssTUFBTWEsQ0FBWCxJQUFnQmIsS0FBaEIsRUFBdUI7QUFDckIsVUFBSWEsQ0FBQyxDQUFDVCxpQkFBRixLQUF3QmtCLFNBQTVCLEVBQXVDO0FBQ3JDLGFBQUtDLGdCQUFMLENBQ0VWLENBQUMsQ0FBQ1osSUFESixFQUVFdUIsS0FBSyxDQUFDQyxPQUFOLENBQWNaLENBQUMsQ0FBQ1QsaUJBQWhCLElBQ0lTLENBQUMsQ0FBQ1QsaUJBRE4sR0FFSSxDQUFDUyxDQUFDLENBQUNULGlCQUFILENBSk47QUFNRDs7QUFFRCxVQUFJUyxDQUFDLENBQUNWLFFBQUYsS0FBZW1CLFNBQW5CLEVBQThCO0FBQzVCLGFBQUtJLGVBQUwsQ0FDRWIsQ0FBQyxDQUFDWixJQURKLEVBRUV1QixLQUFLLENBQUNDLE9BQU4sQ0FBY1osQ0FBQyxDQUFDVixRQUFoQixJQUE0QlUsQ0FBQyxDQUFDVixRQUE5QixHQUF5QyxDQUFDVSxDQUFDLENBQUNWLFFBQUgsQ0FGM0M7QUFJRDs7QUFFRCxZQUFNSSxRQUFRLEdBQUcsSUFBSW9CLEdBQUosRUFBakI7O0FBRUEsVUFBSWQsQ0FBQyxDQUFDWCxVQUFGLEtBQWlCb0IsU0FBckIsRUFBZ0M7QUFDOUIsY0FBTU0sRUFBRSxHQUFHSixLQUFLLENBQUNDLE9BQU4sQ0FBY1osQ0FBQyxDQUFDWCxVQUFoQixJQUE4QlcsQ0FBQyxDQUFDWCxVQUFoQyxHQUE2QyxDQUFDVyxDQUFDLENBQUNYLFVBQUgsQ0FBeEQ7QUFFQTBCLFFBQUFBLEVBQUUsQ0FBQ0MsT0FBSCxDQUFXNUIsSUFBSSxJQUFJO0FBQ2pCLGdCQUFNNkIsSUFBSSxHQUFHLEtBQUtDLE1BQUwsQ0FBWTlCLElBQVosQ0FBYjs7QUFDQSxjQUFJNkIsSUFBSSxLQUFLUixTQUFiLEVBQXdCO0FBQ3RCLGtCQUFNLElBQUlVLEtBQUosQ0FBVyw2QkFBNEIvQixJQUFLLEVBQTVDLENBQU47QUFDRCxXQUZELE1BRU87QUFDTE0sWUFBQUEsUUFBUSxDQUFDMEIsR0FBVCxDQUFhSCxJQUFiO0FBQ0Q7QUFDRixTQVBEO0FBUUQ7O0FBRUQsWUFBTUksRUFBRSxHQUFHLElBQUk3QixHQUFKLENBQVFRLENBQUMsQ0FBQ1osSUFBVixFQUFnQk0sUUFBaEIsQ0FBWDtBQUVBLFdBQUtVLFFBQUwsQ0FBY2tCLEdBQWQsQ0FBa0JELEVBQUUsQ0FBQ2pDLElBQXJCLEVBQTJCaUMsRUFBM0I7QUFDRDtBQUNGO0FBRUQ7Ozs7Ozs7QUFLQUgsRUFBQUEsTUFBTSxDQUFDOUIsSUFBRCxFQUFPO0FBQ1gsV0FBTyxLQUFLZ0IsUUFBTCxDQUFjbUIsR0FBZCxDQUFrQm5DLElBQWxCLENBQVA7QUFDRDtBQUVEOzs7Ozs7O0FBS0FvQyxFQUFBQSxrQkFBa0IsQ0FBQ2xDLFFBQUQsRUFBVztBQUMzQixXQUFPLEtBQUtnQixhQUFMLENBQW1CaUIsR0FBbkIsQ0FBdUJqQyxRQUF2QixDQUFQO0FBQ0Q7QUFFRDs7Ozs7Ozs7O0FBT0FtQyxFQUFBQSxrQkFBa0IsQ0FBQ0MsUUFBRCxFQUFXO0FBQzNCLFVBQU1DLENBQUMsR0FBR0QsUUFBUSxDQUFDRSxLQUFULENBQWUsb0JBQWYsQ0FBVjtBQUNBLFdBQU9ELENBQUMsR0FBRyxLQUFLcEIsc0JBQUwsQ0FBNEJnQixHQUE1QixDQUFnQ0ksQ0FBQyxDQUFDLENBQUQsQ0FBakMsQ0FBSCxHQUEyQ2xCLFNBQW5EO0FBQ0Q7QUFFRDs7Ozs7Ozs7O0FBT0FwQixFQUFBQSxVQUFVLENBQUN3QyxDQUFELEVBQUlDLENBQUosRUFBTztBQUNmLFVBQU1DLEVBQUUsR0FBRyxLQUFLM0IsUUFBTCxDQUFjbUIsR0FBZCxDQUFrQk0sQ0FBbEIsQ0FBWDtBQUNBLFdBQU9FLEVBQUUsS0FBS3RCLFNBQVAsR0FBbUIsS0FBbkIsR0FBMkJzQixFQUFFLENBQUMxQyxVQUFILENBQWMsS0FBS2UsUUFBTCxDQUFjbUIsR0FBZCxDQUFrQk8sQ0FBbEIsQ0FBZCxDQUFsQztBQUNEO0FBRUQ7Ozs7Ozs7O0FBTUFFLEVBQUFBLGtCQUFrQixDQUFDTixRQUFELEVBQVdPLEdBQVgsRUFBZ0I7QUFDaEMsVUFBTUMsSUFBSSxHQUFHLEtBQUtULGtCQUFMLENBQXdCQyxRQUF4QixDQUFiOztBQUNBLFFBQUlRLElBQUksS0FBS3pCLFNBQWIsRUFBd0I7QUFDdEIsYUFBTyxLQUFQO0FBQ0Q7O0FBQ0QsU0FBSyxNQUFNVCxDQUFYLElBQWdCa0MsSUFBaEIsRUFBc0I7QUFDcEIsVUFBSSxLQUFLN0MsVUFBTCxDQUFnQlcsQ0FBaEIsRUFBbUJpQyxHQUFuQixDQUFKLEVBQTZCO0FBQzNCLGVBQU8sSUFBUDtBQUNEO0FBQ0Y7O0FBQ0QsV0FBTyxLQUFQO0FBQ0Q7O0FBRURwQixFQUFBQSxlQUFlLENBQUN6QixJQUFELEVBQU8rQyxRQUFQLEVBQWlCO0FBQzlCQSxJQUFBQSxRQUFRLENBQUNuQixPQUFULENBQWlCb0IsSUFBSSxJQUFJO0FBQ3ZCLFlBQU1wQyxDQUFDLEdBQUcsS0FBS00sYUFBTCxDQUFtQmlCLEdBQW5CLENBQXVCYSxJQUF2QixDQUFWOztBQUNBLFVBQUlwQyxDQUFDLEtBQUtTLFNBQVYsRUFBcUI7QUFDbkIsYUFBS0gsYUFBTCxDQUFtQmdCLEdBQW5CLENBQXVCYyxJQUF2QixFQUE2QixDQUFDaEQsSUFBRCxDQUE3QjtBQUNELE9BRkQsTUFFTztBQUNMWSxRQUFBQSxDQUFDLENBQUNxQyxJQUFGLENBQU9qRCxJQUFQO0FBQ0Q7QUFDRixLQVBEO0FBUUQ7O0FBRURzQixFQUFBQSxnQkFBZ0IsQ0FBQ3RCLElBQUQsRUFBT2tELFVBQVAsRUFBbUI7QUFDakNBLElBQUFBLFVBQVUsQ0FBQ3RCLE9BQVgsQ0FBbUJ1QixHQUFHLElBQUk7QUFDeEIsWUFBTUMsQ0FBQyxHQUFHLEtBQUtqQyxzQkFBTCxDQUE0QmdCLEdBQTVCLENBQWdDZ0IsR0FBaEMsQ0FBVjs7QUFDQSxVQUFJQyxDQUFDLEtBQUsvQixTQUFWLEVBQXFCO0FBQ25CLGFBQUtGLHNCQUFMLENBQTRCZSxHQUE1QixDQUFnQ2lCLEdBQWhDLEVBQXFDLENBQUNuRCxJQUFELENBQXJDO0FBQ0QsT0FGRCxNQUVPO0FBQ0xvRCxRQUFBQSxDQUFDLENBQUNILElBQUYsQ0FBT2pELElBQVA7QUFDRDtBQUNGLEtBUEQ7QUFRRDs7QUE3SXdCOzs7QUFnSjNCLE1BQU1xRCxpQkFBaUIsR0FBRyxJQUFJdEMsYUFBSixFQUExQjtBQUVBc0MsaUJBQWlCLENBQUNqQyxRQUFsQixDQUEyQixDQUN2QjtBQUNJcEIsRUFBQUEsSUFBSSxFQUFFLGtCQURWO0FBRUlDLEVBQUFBLFVBQVUsRUFBRSxDQUFDLGNBQUQsRUFBaUIsWUFBakIsQ0FGaEI7QUFHSUMsRUFBQUEsUUFBUSxFQUFFLHFCQUhkO0FBSUlDLEVBQUFBLGlCQUFpQixFQUFFO0FBSnZCLENBRHVCLEVBT3ZCO0FBQ0lILEVBQUFBLElBQUksRUFBRSwyQkFEVjtBQUVJQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxvQkFBRCxDQUZoQjtBQUdJRSxFQUFBQSxpQkFBaUIsRUFBRTtBQUh2QixDQVB1QixFQVl2QjtBQUNJSCxFQUFBQSxJQUFJLEVBQUUsVUFEVjtBQUVJQyxFQUFBQSxVQUFVLEVBQUUsQ0FBQyxhQUFELENBRmhCO0FBR0lFLEVBQUFBLGlCQUFpQixFQUFFLENBQUUsTUFBRixFQUFVLE9BQVYsQ0FIdkI7QUFJSUQsRUFBQUEsUUFBUSxFQUFFLENBQUUsa0JBQUYsRUFBc0IsV0FBdEI7QUFKZCxDQVp1QixDQUEzQjtlQW9CZW1ELGlCIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgdHlwZXMgPSBbXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5pdGVtXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmNvbnRlbnRcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMubWVzc2FnZVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5jb250YWN0XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmNvbXBvc2l0ZS1jb250ZW50XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuY29udGVudFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5kYXRhXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuaXRlbVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5kYXRhYmFzZVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5kaXJlY3RvcnlcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5pdGVtXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmV4ZWN1dGFibGVcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5pdGVtXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLm1pY3Jvc29mdC53aW5kb3dzLWR5bmFtaWMtbGluay1saWJyYXJ5XCIsXG4gICAgY29uZm9ybXNUbzogW1wicHVibGljLmV4ZWN1dGFibGVcIiwgXCJwdWJsaWMuZGF0YVwiXSxcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi94LW1zZG93bmxvYWRcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuZGxsXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLm1pY3Jvc29mdC53aW5kb3dzLWV4ZWN1dGFibGVcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZXhlY3V0YWJsZVwiLCBcInB1YmxpYy5kYXRhXCJdLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gtbXNkb3dubG9hZFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5leGVcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMub2JqZWN0LWNvZGVcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZXhlY3V0YWJsZVwiLCBcInB1YmxpYy5kYXRhXCJdLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5vXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnVuaXgtZXhlY3V0YWJsZVwiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5leGVjdXRhYmxlXCIsIFwicHVibGljLmRhdGFcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLnN1bi5qYXZhLWNsYXNzXCIsXG4gICAgY29uZm9ybXNUbzogW1wicHVibGljLmV4ZWN1dGFibGVcIiwgXCJwdWJsaWMuZGF0YVwiXSxcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuY2xhc3NcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMua2V5LXZhbHVlXCIsXG4gICAgY29uZm9ybXNUbzogW1wicHVibGljLmRhdGFcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLnN1bi5qYXZhLXByb3BlcnRpZXNcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMua2V5LXZhbHVlXCJdLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5wcm9wZXJ0aWVzXCIsXG4gICAgbWltZVR5cGU6IFwidGV4dC94LWphdmEtcHJvcGVydGllc1wiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy56aXAtYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuemlwXCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24vemlwXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLnN1bi5qYXZhLWtleXN0b3JlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5qa3NcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJjb20uc3VuLmphdmEtYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5leGVjdXRhYmxlXCIsIFwicHVibGljLnppcC1hcmNoaXZlXCJdLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gtamF2YS1hcmNoaXZlXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLmphclwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5zdW4uamF2YS13ZWItYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5leGVjdXRhYmxlXCIsIFwicHVibGljLnppcC1hcmNoaXZlXCJdLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi53YXJcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJjb20uc3VuLmphdmEtZW50ZXJwcmlzZS1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogW1wicHVibGljLmV4ZWN1dGFibGVcIiwgXCJwdWJsaWMuemlwLWFyY2hpdmVcIl0sXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLmVhclwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5mb2xkZXJcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5kaXJlY3RvcnlcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMudm9sdW1lXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZm9sZGVyXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLmFwcGxlLnJlc29sdmFibGVcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuc3ltbGlua1wiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5pdGVtXCIsIFwiY29tLmFwcGxlLnJlc29sdmFibGVcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnVybFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmRhdGFcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuZmlsZS11cmxcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy51cmxcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMudGV4dFwiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5kYXRhXCIsIFwicHVibGljLmNvbnRlbnRcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnBsYWluLXRleHRcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy50ZXh0XCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLnR4dFwiLFxuICAgIG1pbWVUeXBlOiBcInRleHQvcGxhaW5cIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMudXRmOC1wbGFpbi10ZXh0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMucGxhaW4tdGV4dFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy51dGYxNi1leHRlcm5hbC1wbGFpbi3igIt0ZXh0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMucGxhaW4tdGV4dFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy51dGYxNi1wbGFpbi10ZXh0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMucGxhaW4tdGV4dFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5ydGZcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy50ZXh0XCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLnJ0ZlwiLFxuICAgIG1pbWVUeXBlOiBcInRleHQvcnRmXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmh0bWxcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy50ZXh0XCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFtcIi5odG1sXCIsIFwiLmh0bVwiXSxcbiAgICBtaW1lVHlwZTogXCJ0ZXh0L2h0bWxcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMueG1sXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMudGV4dFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi54bWxcIixcbiAgICBtaW1lVHlwZTogXCJ0ZXh0L3htbFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5qc29uXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMudGV4dFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5qc29uXCIsXG4gICAgbWltZVR5cGU6IFtcImFwcGxpY2F0aW9uL2pzb25cIiwgXCJ0ZXh0L2pzb25cIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnRvbWxcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy50ZXh0XCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLnRvbWxcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi90b21sXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmluaVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnRleHRcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuaW5pXCIsXG4gICAgbWltZVR5cGU6IFwienotYXBwbGljYXRpb24venotd2luYXNzb2MtaW5pXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnNvdXJjZS1jb2RlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMucGxhaW4tdGV4dFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5zY3JpcHRcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5zb3VyY2UtY29kZVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5zdW4uamF2YS1zb3VyY2VcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5zb3VyY2UtY29kZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIuamF2YVwiLCBcIi5qYXZcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnNoZWxsLXNjcmlwdFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnNjcmlwdFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIuc2hcIiwgXCIuY29tbWFuZFwiXSxcbiAgICBtaW1lVHlwZTogW1widGV4dC94LXNoZWxsc2NyaXB0XCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5iYXNoLXNjcmlwdFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnNoZWxsLXNjcmlwdFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIuYmFzaFwiXVxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMua3NoLXNjcmlwdFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnNoZWxsLXNjcmlwdFwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIua3NoXCJdLFxuICAgIG1pbWVUeXBlOiBbXCJ0ZXh0L3gtc2NyaXB0LmtzaFwiLCBcImFwcGxpY2F0aW9uL3gta3NoXCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5hc3NlbWJseS1zb3VyY2VcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5zb3VyY2UtY29kZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5zXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLm5ldHNjYXBlLmphdmFzY3JpcHQtc291cmNlXCIsXG4gICAgY29uZm9ybXNUbzogW1wicHVibGljLnNvdXJjZS1jb2RlXCIsIFwicHVibGljLmV4ZWN1dGFibGVcIl0sXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFtcIi5qc1wiLCBcIi5qc2NyaXB0XCIsIFwiLmphdmFzY3JpcHRcIl0sXG4gICAgbWltZVR5cGU6IFtcImFwcGxpY2F0aW9uL2phdmFzY3JpcHRcIiwgXCJhcHBsaWNhdGlvbi9lY21hc2NyaXB0XCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5iemlwMi1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5iemlwMlwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIm9yZy5nbnUuZ251LXRhci1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi50YXJcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi94LXRhclwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy50YXItYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwib3JnLmdudS5nbnUtdGFyLWFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIudGFyXCIsXG4gICAgbWltZVR5cGU6IFtcImFwcGxpY2F0aW9uL3gtdGFyXCIsIFwiYXBwbGljYXRpb24vdGFyXCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIm9yZy5nbnUuZ251LXppcC1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIuZ3pcIiwgXCIuZ3ppcFwiXSxcbiAgICBtaW1lVHlwZTogW1wiYXBwbGljYXRpb24vZ3ppcFwiLCBcImFwcGxpY2F0aW9uL3gtZ3ppcFwiXVxuICB9LFxuICB7XG4gICAgbmFtZTogXCJvcmcuZ251LmdudS16aXAtdGFyLWFyY2hpdmVcIixcbiAgICBjb25mb3Jtc1RvOiBcIm9yZy5nbnUuZ251LXppcC1hcmNoaXZlXCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24vdGFyK2d6aXBcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLnRhci5nelwiLCBcIi50Z3pcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLmFwcGxlLnhhci1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi54YXJcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMueHotYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIueHpcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi94LXh6XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLjd6LWFyY2hpdmVcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5hcmNoaXZlXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLjd6XCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24veC03ei1jb21wcmVzc2VkXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnJhci1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5yYXJcIixcbiAgICBtaW1lVHlwZTogW1wiYXBwbGljYXRpb24veC1yYXItY29tcHJlc3NlZFwiLCBcImFwcGxpY2F0aW9uL3ZuZC5yYXJcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmNwaW8tYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuY3Bpb1wiLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gtY3Bpb1wiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5ycG0tYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIucnBtXCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24veC1ycG1cIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuYXItYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLmFcIixcIi5saWJcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmRlYi1hcmNoaXZlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXItYXJjaGl2ZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5kZWJcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi92bmQuZGViaWFuLmJpbmFyeS1wYWNrYWdlXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmFyY2gtbGludXgtYXJjaGl2ZVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmFyY2hpdmVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIucGtnLnRhci54elwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5hcHBsZS5wYWNrYWdlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGlyZWN0b3J5XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLmFwcGxlLmFwcGxpY2F0aW9uXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5hcHBsZS5hcHBsaWNhdGlvbi1idW5kbGVcIixcbiAgICBjb25mb3Jtc1RvOiBcImNvbS5hcHBsZS5hcHBsaWNhdGlvblwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXG4gICAgICBcIi5hcHBcIixcbiAgICAgIFwiLmZyYW1ld29ya1wiLFxuICAgICAgXCIua2V4dFwiLFxuICAgICAgXCIucGx1Z2luXCIsXG4gICAgICBcIi5kb2NzZXRcIixcbiAgICAgIFwiLnhwY1wiLFxuICAgICAgXCIucWxnZW5lcmF0b3JcIixcbiAgICAgIFwiLmNvbXBvbmVudFwiLFxuICAgICAgXCIuc2F2ZXJcIixcbiAgICAgIFwiLm1kaW1wb3J0XCJcbiAgICBdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5kaXNrLWltYWdlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuYXJjaGl2ZVwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5pbWFnZVwiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5kYXRhXCIsIFwicHVibGljLmNvbnRlbnRcIl1cbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLmNhc2UtaW5zZW5zaXRpdmUtdGV4dFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5maWxlbmFtZS1leHRlbnNpb25cIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5jYXNlLWluc2Vuc2l0aXZlLXRleHRcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMubWltZS10eXBlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuY2FzZS1pbnNlbnNpdGl2ZS10ZXh0XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnByZXNlbnRhdGlvblwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmNvbXBvc2l0ZS1jb250ZW50XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLm1pY3Jvc29mdC53b3JkLmRvY1wiLFxuICAgIGNvbmZvcm1zVG86IFtcInB1YmxpYy5kYXRhXCIsIFwicHVibGljLmNvbXBvc2l0ZS1jb250ZW50XCJdLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5kb2N4XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiY29tLm1pY3Jvc29mdC5leGNlbC54bHNcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZGF0YVwiLCBcInB1YmxpYy5jb21wb3NpdGUtY29udGVudFwiXSxcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIueGxzeFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5taWNyb3NvZnQucG93ZXJwb2ludC5wcHRcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZGF0YVwiLCBcInB1YmxpYy5wcmVzZW50YXRpb25cIl0sXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLnBwdFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5taWNyb3NvZnQud29yZC53b3JkbWxcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZGF0YVwiLCBcInB1YmxpYy5jb21wb3NpdGUtY29udGVudFwiXVxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuaW1hZ2VcIixcbiAgICBjb25mb3Jtc1RvOiBbXCJwdWJsaWMuZGF0YVwiLCBcInB1YmxpYy5jb250ZW50XCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5qcGVnXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuaW1hZ2VcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLmpwZ1wiLCBcIi5qcGVnXCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5hZG9iZS5wb3N0c2NyaXB0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuaW1hZ2VcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLnBzXCJdLFxuICAgIG1pbWVUeXBlOiBbXCJhcHBsaWNhdGlvbi9wb3N0c2NyaXB0XCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIm5ldC5kYXJpbmdmaXJlYmFsbC5tYXJrZG93blwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnRleHRcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLm1kXCIsIFwiLm1hcmtkb3duXCJdLFxuICAgIG1pbWVUeXBlOiBbXCJ0ZXh0L21hcmtkb3duXCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcImNvbS5hcHBsZS5kaXNrLWltYWdlXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGlzay1pbWFnZVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIuZG1nXCIsIFwiLnNtaVwiLCBcIi5pbWdcIl0sXG4gICAgbWltZVR5cGU6IFtcImFwcGxpY2F0aW9uL3gtYXBwbGUtZGlza2ltYWdlXCJdXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5zZWN1cml0eS5wcml2YXRlLWtleVwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmRhdGFcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGVcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5kYXRhXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnNlY3VyaXR5LmNlcnRpZmljYXRlLnRydXN0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGVcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGUucmVxdWVzdFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmRhdGFcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMucGtjczhcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5kYXRhXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFtcIi5wOFwiLCBcIi5rZXlcIl0sXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24vcGtjczhcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMucGtjczEwXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBbXCIucDEwXCIsIFwiLmNzclwiXSxcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi9wa2NzMTBcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMucGtpeC1jZXJ0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIuY2VyXCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24vcGtpeC1jZXJ0XCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLnBraXgtY3JsXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5jcmxcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi9wa2l4LWNybFwiXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcInB1YmxpYy5wa2NzNy1taW1lXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5wN2NcIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi9wa2NzNy1taW1lXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLngteDUwOS1jYS1jZXJ0XCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLmNydFwiLCBcIi5kZXJcIl0sXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24veC14NTA5LWNhLWNlcnRcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMueC14NTA5LXVzZXItY2VydFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLnNlY3VyaXR5LmNlcnRpZmljYXRlXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLmNydFwiLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gteDUwOS11c2VyLWNlcnRcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMueC1wa2NzNy1jcmxcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5kYXRhXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFwiLmNybFwiLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gtcGtjczctY3JsXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLngtcGtjczEyXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuZGF0YVwiLFxuICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiBcIi5wZW1cIixcbiAgICBtaW1lVHlwZTogXCJhcHBsaWNhdGlvbi94LXBlbS1maWxlXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLngtcGVtLWZpbGVcIixcbiAgICBjb25mb3Jtc1RvOiBcInB1YmxpYy5kYXRhXCIsXG4gICAgZmlsZU5hbWVFeHRlbnNpb246IFtcIi5wMTJcIiwgXCIucGZ4XCJdLFxuICAgIG1pbWVUeXBlOiBcImFwcGxpY2F0aW9uL3gtcGtjczEyXCJcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwicHVibGljLngtcGtjczctY2VydGlmaWNhdGVzXCIsXG4gICAgY29uZm9ybXNUbzogXCJwdWJsaWMuc2VjdXJpdHkuY2VydGlmaWNhdGVcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogW1wiLnA3YlwiLCBcIi5zcGNcIl0sXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24veC1wa2NzNy1jZXJ0aWZpY2F0ZXNcIlxuICB9LFxuICB7XG4gICAgbmFtZTogXCJwdWJsaWMueC1wa2NzNy1jZXJ0cmVxcmVzcFwiLFxuICAgIGNvbmZvcm1zVG86IFwicHVibGljLmRhdGFcIixcbiAgICBmaWxlTmFtZUV4dGVuc2lvbjogXCIucDdyXCIsXG4gICAgbWltZVR5cGU6IFwiYXBwbGljYXRpb24veC1wa2NzNy1jZXJ0cmVxcmVzcFwiXG4gIH1cbl07XG5cbi8qKlxuICogT2JqZWN0IHJlcHJlc2VudGluZyBhIFVUSVxuICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAqIEBwYXJhbSB7c3RyaW5nfSBjb25mb3Jtc1xuICpcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBuYW1lXG4gKiBAcHJvcGVydHkge3N0cmluZ30gY29uZm9ybXNcbiAqL1xuY2xhc3MgVVRJIHtcbiAgY29uc3RydWN0b3IobmFtZSwgY29uZm9ybXMpIHtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0aGlzLCB7XG4gICAgICBuYW1lOiB7XG4gICAgICAgIHZhbHVlOiBuYW1lXG4gICAgICB9LFxuICAgICAgY29uZm9ybXM6IHtcbiAgICAgICAgdmFsdWU6IGNvbmZvcm1zXG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgZm9yIGNvbmZvcm1pdHlcbiAgICogQHBhcmFtIHtVVEl9IG90aGVyXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59IHRydWUgaWYgb3RoZXIgY29uZm9ybXMgdG8gdGhlIHJlY2VpdmVyXG4gICAqL1xuICBjb25mb3Jtc1RvKG90aGVyKSB7XG4gICAgaWYgKHRoaXMgPT09IG90aGVyIHx8IHRoaXMuY29uZm9ybXMuaGFzKG90aGVyKSkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgZm9yIChjb25zdCB1IG9mIHRoaXMuY29uZm9ybXMpIHtcbiAgICAgIGlmICh1LmNvbmZvcm1zVG8ob3RoZXIpKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB0b1N0cmluZygpIHtcbiAgICByZXR1cm4gdGhpcy5uYW1lO1xuICB9XG5cbiAgLyoqXG4gICAqIERlbGl2ZXIgSlNPTiByZXByZXNlbnRhdGlvbiBvZiB0aGUgVVRJLlxuICAgKiBTYW1wbGUgcmVzdWx0XG4gICAqIMK0wrTCtGpzb25cbiAgICoge1xuICAgKiAgIFwibmFtZVwiOiBcIm15VVRJXCIsXG4gICAqICAgXCJjb25mb3Jtc1RvXCI6IFsgXCJ1dGkxXCIsIFwidXRpMlwiXVxuICAgKiB9XG4gICAqIMK0wrTCtFxuICAgKiBAcmV0dXJuIHtPYmplY3R9IGpzb24gcmVwcmVzZW50YXRpb24gb2YgdGhlIFVUSVxuICAgKi9cbiAgdG9KU09OKCkge1xuICAgIHJldHVybiB7XG4gICAgICBuYW1lOiB0aGlzLm5hbWUsXG4gICAgICBjb25mb3JtczogdGhpcy5jb25mb3Jtc1xuICAgIH07XG4gIH1cbn1cblxuLyoqXG4gKiBSZWdpc3RyeSBvZiBVVElzXG4gKiBAcHJvcGVydHkge01hcDxzdHJpbmcsVVRJPn0gcmVnaXN0cnlcbiAqIEBwcm9wZXJ0eSB7TWFwPHN0cmluZyxVVEk+fSB1dGlCeU1pbWVUeXBlXG4gKiBAcHJvcGVydHkge01hcDxzdHJpbmcsVVRJPn0gdXRpQnlGaWxlTmFtZUV4dGVuc2lvblxuICovXG5leHBvcnQgY2xhc3MgVVRJQ29udHJvbGxlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHRoaXMsIHtcbiAgICAgIHJlZ2lzdHJ5OiB7XG4gICAgICAgIHZhbHVlOiBuZXcgTWFwKClcbiAgICAgIH0sXG4gICAgICB1dGlCeU1pbWVUeXBlOiB7XG4gICAgICAgIHZhbHVlOiBuZXcgTWFwKClcbiAgICAgIH0sXG4gICAgICB1dGlCeUZpbGVOYW1lRXh0ZW5zaW9uOiB7XG4gICAgICAgIHZhbHVlOiBuZXcgTWFwKClcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMucmVnaXN0ZXIodHlwZXMpO1xuICB9XG5cbiAgLyoqXG4gICAqIHJlZ2lzdGVycyBhZGRpdGlvbmFsIHR5cGVzXG4gICAqIEBwYXJhbSB7T2JqZWN0W119IHR5cGVzXG4gICAqL1xuICByZWdpc3Rlcih0eXBlcykge1xuICAgIGZvciAoY29uc3QgdSBvZiB0eXBlcykge1xuICAgICAgaWYgKHUuZmlsZU5hbWVFeHRlbnNpb24gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLmFzc2lnbkV4dGVuc2lvbnMoXG4gICAgICAgICAgdS5uYW1lLFxuICAgICAgICAgIEFycmF5LmlzQXJyYXkodS5maWxlTmFtZUV4dGVuc2lvbilcbiAgICAgICAgICAgID8gdS5maWxlTmFtZUV4dGVuc2lvblxuICAgICAgICAgICAgOiBbdS5maWxlTmFtZUV4dGVuc2lvbl1cbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHUubWltZVR5cGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLmFzc2lnbk1pbWVUeXBlcyhcbiAgICAgICAgICB1Lm5hbWUsXG4gICAgICAgICAgQXJyYXkuaXNBcnJheSh1Lm1pbWVUeXBlKSA/IHUubWltZVR5cGUgOiBbdS5taW1lVHlwZV1cbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgY29uZm9ybXMgPSBuZXcgU2V0KCk7XG5cbiAgICAgIGlmICh1LmNvbmZvcm1zVG8gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjb25zdCBjdCA9IEFycmF5LmlzQXJyYXkodS5jb25mb3Jtc1RvKSA/IHUuY29uZm9ybXNUbyA6IFt1LmNvbmZvcm1zVG9dO1xuXG4gICAgICAgIGN0LmZvckVhY2gobmFtZSA9PiB7XG4gICAgICAgICAgY29uc3QgYVVUSSA9IHRoaXMuZ2V0VVRJKG5hbWUpO1xuICAgICAgICAgIGlmIChhVVRJID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgUmVmZXJlbmNlZCBVVEkgbm90IGtub3duOiAke25hbWV9YCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbmZvcm1zLmFkZChhVVRJKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBudSA9IG5ldyBVVEkodS5uYW1lLCBjb25mb3Jtcyk7XG5cbiAgICAgIHRoaXMucmVnaXN0cnkuc2V0KG51Lm5hbWUsIG51KTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTG9va3VwIGEgZ2l2ZW4gVVRJLlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBVVElcbiAgICogQHJldHVybiB7c3RyaW5nfSBVVEkgZm9yIHRoZSBnaXZlbiBuYW1lIG9yIHVuZGVmaW5lZCBpZiBVVEkgaXMgbm90IHByZXNlbnQuXG4gICAqL1xuICBnZXRVVEkobmFtZSkge1xuICAgIHJldHVybiB0aGlzLnJlZ2lzdHJ5LmdldChuYW1lKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb29rdXAgYSBVVElzIGZvciBhIG1pbWUgdHlwZS5cbiAgICogQHBhcmFtIHtzdHJpbmd9IG1pbWVUeXBlIG1pbWUgdHlwZSB0byBnZXQgVVRJcyBmb3JcbiAgICogQHJldHVybiB7c3RyaW5nfSBVVEkgZm9yIHRoZSBnaXZlbiBtaW1lIHR5cGUgb3IgdW5kZWZpbmVkIGlmIG5vIFVUSSBpcyByZWdpc3RlcmQgZm9yIHRoZSBtaW1lIHR5cGVcbiAgICovXG4gIGdldFVUSXNGb3JNaW1lVHlwZShtaW1lVHlwZSkge1xuICAgIHJldHVybiB0aGlzLnV0aUJ5TWltZVR5cGUuZ2V0KG1pbWVUeXBlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb29rdXAgYSBVVEkgZm9yIGEgZmlsZSBuYW1lLlxuICAgKiBGaXJzdCB0aGUgZmlsZSBuYW1lIGV4dGVuc2lvbiBpcyBleHRyYWN0ZWQuXG4gICAqIFRoZW4gYSBsb29rdXAgaW4gdGhlIHJlaXN0ZXJlZCBVVElzIGZvciBmaWxlIG5hbWUgZXh0aW9ucyBpcyBleGVjdXRlZC5cbiAgICogQHBhcmFtIHtzdHJpbmd9IGZpbGVOYW1lIGZpbGUgdG8gZGV0ZWN0IFVUSSBmb3JcbiAgICogQHJldHVybiB7c3RyaW5nfSBVVEkgZm9yIHRoZSBnaXZlbiBmaWxlTmFtZSBvciB1bmRlZmluZWQgaWYgbm8gVVRJIGlzIHJlZ2lzdGVyZCBmb3IgdGhlIGZpbGUgbmFtZXMgZXh0ZW5zaW9uXG4gICAqL1xuICBnZXRVVElzRm9yRmlsZU5hbWUoZmlsZU5hbWUpIHtcbiAgICBjb25zdCBtID0gZmlsZU5hbWUubWF0Y2goLyhcXC5bYS16QS1aXzAtOV0rKSQvKTtcbiAgICByZXR1cm4gbSA/IHRoaXMudXRpQnlGaWxlTmFtZUV4dGVuc2lvbi5nZXQobVsxXSkgOiB1bmRlZmluZWQ7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgd2hlbmV2ZXIgdHdvIFVUSSBhcmUgY29uZm9ybWFudC5cbiAgICogSWYgYSBjb25mb3JtcyB0byBiIGFuZCBiIGNvbmZvcm1zIHRvIGMgdGhlbiBhIGFsc28gY29uZm9ybXMgdG8gYy5cbiAgICogQHBhcmFtIHtzdHJpbmd9IGEgZmlyc3QgVVRJXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBiIHNlY29uZCBVVElcbiAgICogQHJldHVybiB7Ym9vbGVhbn0gdHJ1ZSBpZiBVVEkgYSBjb25mb3JtcyB0byBVVEkgYi5cbiAgICovXG4gIGNvbmZvcm1zVG8oYSwgYikge1xuICAgIGNvbnN0IHVhID0gdGhpcy5yZWdpc3RyeS5nZXQoYSk7XG4gICAgcmV0dXJuIHVhID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IHVhLmNvbmZvcm1zVG8odGhpcy5yZWdpc3RyeS5nZXQoYikpO1xuICB9XG5cbiAgLyoqXG4gICAqIExvb2t1cCBhIFVUSSBmb3IgYSBmaWxlIG5hbWUgYW5kIGNoZWNrIGNvbmZvcm1hbmNlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBmaWxlTmFtZSBmaWxlIHRvIGRldGVjdCBVVEkgZm9yXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB1dGkgdG8gY2hlY2sgY29uZm9ybWFuY2UgZWdhaW5zdFxuICAgKiBAcmV0dXJuIHtib29sZWFufSB0dXJlIGlmIHV0aWxzIGZvciBmaWxlIG5hbWUgYXJlIGNvbmZvcm1hbnRcbiAgICovXG4gIGZpbGVOYW1lQ29uZm9ybXNUbyhmaWxlTmFtZSwgdXRpKSB7XG4gICAgY29uc3QgdXRpcyA9IHRoaXMuZ2V0VVRJc0ZvckZpbGVOYW1lKGZpbGVOYW1lKTtcbiAgICBpZiAodXRpcyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGZvciAoY29uc3QgdSBvZiB1dGlzKSB7XG4gICAgICBpZiAodGhpcy5jb25mb3Jtc1RvKHUsIHV0aSkpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGFzc2lnbk1pbWVUeXBlcyhuYW1lLCBtaW1UeXBlcykge1xuICAgIG1pbVR5cGVzLmZvckVhY2godHlwZSA9PiB7XG4gICAgICBjb25zdCB1ID0gdGhpcy51dGlCeU1pbWVUeXBlLmdldCh0eXBlKTtcbiAgICAgIGlmICh1ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdGhpcy51dGlCeU1pbWVUeXBlLnNldCh0eXBlLCBbbmFtZV0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdS5wdXNoKG5hbWUpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgYXNzaWduRXh0ZW5zaW9ucyhuYW1lLCBleHRlbnNpb25zKSB7XG4gICAgZXh0ZW5zaW9ucy5mb3JFYWNoKGV4dCA9PiB7XG4gICAgICBjb25zdCBlID0gdGhpcy51dGlCeUZpbGVOYW1lRXh0ZW5zaW9uLmdldChleHQpO1xuICAgICAgaWYgKGUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLnV0aUJ5RmlsZU5hbWVFeHRlbnNpb24uc2V0KGV4dCwgW25hbWVdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGUucHVzaChuYW1lKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxufVxuXG5jb25zdCBkZWZhdWx0Q29udHJvbGxlciA9IG5ldyBVVElDb250cm9sbGVyKClcblxuZGVmYXVsdENvbnRyb2xsZXIucmVnaXN0ZXIoW1xuICAgIHtcbiAgICAgICAgbmFtZTogJ3B1YmxpYy5zdmctaW1hZ2UnLFxuICAgICAgICBjb25mb3Jtc1RvOiBbJ3B1YmxpYy5pbWFnZScsICdwdWJsaWMueG1sJ10sXG4gICAgICAgIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vc3ZnK3htbCcsXG4gICAgICAgIGZpbGVOYW1lRXh0ZW5zaW9uOiAnLnN2ZydcbiAgICB9LFxuICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbS5taWNyb3Nvc2Z0LnR5cGVzY3JpcHQnLFxuICAgICAgICBjb25mb3Jtc1RvOiBbJ3B1YmxpYy5zb3VyY2UtY29kZSddLFxuICAgICAgICBmaWxlTmFtZUV4dGVuc2lvbjogJy50cydcbiAgICB9LFxuICAgIHtcbiAgICAgICAgbmFtZTogJ29yZy55YW1sJyxcbiAgICAgICAgY29uZm9ybXNUbzogWydwdWJsaWMudGV4dCddLFxuICAgICAgICBmaWxlTmFtZUV4dGVuc2lvbjogWyAnLnltbCcsICcueWFtbCcgXSxcbiAgICAgICAgbWltZVR5cGU6IFsgJ2FwcGxpY2F0aW9uL3lhbWwnLCAndGV4dC95YW1sJyBdXG4gICAgfVxuXSlcblxuZXhwb3J0IGRlZmF1bHQgZGVmYXVsdENvbnRyb2xsZXIiXX0=