import pino from 'pino'
import pinoPretty from 'pino-pretty'

export function createLogger() {
    return pino({ prettyPrint: { levelFirst: true }, prettifier: pinoPretty })
}