import _ from 'lodash'
import { PQueue } from 'p-queue'

export interface Task <Context, FlowThrough> {

    execute (ctx: Context, thru: FlowThrough): Promise<FlowThrough>

}

export interface TaskFunc <Context, FlowThrough> {
    (ctx: Context, thru: FlowThrough): FlowThrough
}

export interface TaskFuncAsync <Context, FlowThrough> {
    (ctx: Context, thru: FlowThrough): Promise<FlowThrough>
}

function taskIsFunc (task: Task<Context, FlowThrough> | TaskFunc<Context, FlowThrough> | TaskFuncAsync<Context, FlowThrough>): task is TaskFunc<Context, FlowThrough> {
    return _.isFunction(task)
}

function taskIsFuncAsync (task: Task<Context, FlowThrough> | TaskFunc<Context, FlowThrough> | TaskFuncAsync<Context, FlowThrough>): task is TaskFuncAsync<Context, FlowThrough> {
    return (task instanceof Object.getPrototypeOf(async function () {}).constructor)
}

export class SimpleTask<Context, FlowThrough> implements Task<Context, FlowThrough> {

    private _fn: TaskFunc | TaskFuncAsync
    private _isAsync: bool

    constructor (fn: TaskFunc | TaskFuncAsync) {
        this._fn = fn
        this._isAsync = taskIsFuncAsync(fn)
    }

    execute (ctx: Context, thru: FlowThrough): Promise<FlowThrough> {
        if (this._isAsync) {
            return this._fn(ctx, thru)
        } else {
            return Promise.resolve(this._fn(ctx, thru))
        }
    }

}

export class Flow <Context, FlowThrough> implements Task <Context, FlowThrough> {


    readonly workQueue: PQueue
    private _tasks: Task<Context, FlowThrough>[]

    constructor (workQueue?: PQueue) {
        this._tasks = []
        this.workQueue = workQueue || new PQueue();
    }

    execute (ctx: Context, thru: FlowThrough): Promise<FlowThrough> {

        const workQueue = this.workQueue

        return _.reduce(this._tasks, (pr, task) => pr.then(t => workQueue.add(_.bind(task.execute, task, ctx, t))), Promise.resolve(thru))

    }

    add (task: Task<Context, FlowThrough> | TaskFunc<Context, FlowThrough>): Flow<Context, FlowThrough> {
        if (taskIsFunc(task)) {
            this._tasks.push(new SimpleTask(task))
        } else {
            this._tasks.push(task)
        }

        return this;
    }

}