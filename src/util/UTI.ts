const types = [
  {
    name: "public.item"
  },
  {
    name: "public.content"
  },
  {
    name: "public.message"
  },
  {
    name: "public.contact"
  },
  {
    name: "public.composite-content",
    conformsTo: "public.content"
  },
  {
    name: "public.data",
    conformsTo: "public.item"
  },
  {
    name: "public.database"
  },
  {
    name: "public.archive",
    conformsTo: "public.data"
  },
  {
    name: "public.directory",
    conformsTo: "public.item"
  },
  {
    name: "public.executable",
    conformsTo: "public.item"
  },
  {
    name: "com.microsoft.windows-dynamic-link-library",
    conformsTo: ["public.executable", "public.data"],
    mimeType: "application/x-msdownload",
    fileNameExtension: ".dll"
  },
  {
    name: "com.microsoft.windows-executable",
    conformsTo: ["public.executable", "public.data"],
    mimeType: "application/x-msdownload",
    fileNameExtension: ".exe"
  },
  {
    name: "public.object-code",
    conformsTo: ["public.executable", "public.data"],
    fileNameExtension: ".o"
  },
  {
    name: "public.unix-executable",
    conformsTo: ["public.executable", "public.data"]
  },
  {
    name: "com.sun.java-class",
    conformsTo: ["public.executable", "public.data"],
    fileNameExtension: ".class"
  },
  {
    name: "public.key-value",
    conformsTo: ["public.data"]
  },
  {
    name: "com.sun.java-properties",
    conformsTo: ["public.key-value"],
    fileNameExtension: ".properties",
    mimeType: "text/x-java-properties"
  },
  {
    name: "public.zip-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".zip",
    mimeType: "application/zip"
  },
  {
    name: "com.sun.java-keystore",
    conformsTo: "public.archive",
    fileNameExtension: ".jks"
  },
  {
    name: "com.sun.java-archive",
    conformsTo: ["public.executable", "public.zip-archive"],
    mimeType: "application/x-java-archive",
    fileNameExtension: ".jar"
  },
  {
    name: "com.sun.java-web-archive",
    conformsTo: ["public.executable", "public.zip-archive"],
    fileNameExtension: ".war"
  },
  {
    name: "com.sun.java-enterprise-archive",
    conformsTo: ["public.executable", "public.zip-archive"],
    fileNameExtension: ".ear"
  },
  {
    name: "public.folder",
    conformsTo: "public.directory"
  },
  {
    name: "public.volume",
    conformsTo: "public.folder"
  },
  {
    name: "com.apple.resolvable"
  },
  {
    name: "public.symlink",
    conformsTo: ["public.item", "com.apple.resolvable"]
  },
  {
    name: "public.url",
    conformsTo: "public.data"
  },
  {
    name: "public.file-url",
    conformsTo: "public.url"
  },
  {
    name: "public.text",
    conformsTo: ["public.data", "public.content"]
  },
  {
    name: "public.plain-text",
    conformsTo: "public.text",
    fileNameExtension: ".txt",
    mimeType: "text/plain"
  },
  {
    name: "public.utf8-plain-text",
    conformsTo: "public.plain-text"
  },
  {
    name: "public.utf16-external-plain-​text",
    conformsTo: "public.plain-text"
  },
  {
    name: "public.utf16-plain-text",
    conformsTo: "public.plain-text"
  },
  {
    name: "public.rtf",
    conformsTo: "public.text",
    fileNameExtension: ".rtf",
    mimeType: "text/rtf"
  },
  {
    name: "public.html",
    conformsTo: "public.text",
    fileNameExtension: [".html", ".htm"],
    mimeType: "text/html"
  },
  {
    name: "public.xml",
    conformsTo: "public.text",
    fileNameExtension: ".xml",
    mimeType: "text/xml"
  },
  {
    name: "public.json",
    conformsTo: "public.text",
    fileNameExtension: ".json",
    mimeType: ["application/json", "text/json"]
  },
  {
    name: "public.toml",
    conformsTo: "public.text",
    fileNameExtension: ".toml",
    mimeType: "application/toml"
  },
  {
    name: "public.ini",
    conformsTo: "public.text",
    fileNameExtension: ".ini",
    mimeType: "zz-application/zz-winassoc-ini"
  },
  {
    name: "public.source-code",
    conformsTo: "public.plain-text"
  },
  {
    name: "public.script",
    conformsTo: "public.source-code"
  },
  {
    name: "com.sun.java-source",
    conformsTo: "public.source-code",
    fileNameExtension: [".java", ".jav"]
  },
  {
    name: "public.shell-script",
    conformsTo: "public.script",
    fileNameExtension: [".sh", ".command"],
    mimeType: ["text/x-shellscript"]
  },
  {
    name: "public.bash-script",
    conformsTo: "public.shell-script",
    fileNameExtension: [".bash"]
  },
  {
    name: "public.ksh-script",
    conformsTo: "public.shell-script",
    fileNameExtension: [".ksh"],
    mimeType: ["text/x-script.ksh", "application/x-ksh"]
  },
  {
    name: "public.assembly-source",
    conformsTo: "public.source-code",
    fileNameExtension: ".s"
  },
  {
    name: "com.netscape.javascript-source",
    conformsTo: ["public.source-code", "public.executable"],
    fileNameExtension: [".js", ".jscript", ".javascript"],
    mimeType: ["application/javascript", "application/ecmascript"]
  },
  {
    name: "public.bzip2-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".bzip2"
  },
  {
    name: "org.gnu.gnu-tar-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".tar",
    mimeType: "application/x-tar"
  },
  {
    name: "public.tar-archive",
    conformsTo: "org.gnu.gnu-tar-archive",
    fileNameExtension: ".tar",
    mimeType: ["application/x-tar", "application/tar"]
  },
  {
    name: "org.gnu.gnu-zip-archive",
    conformsTo: "public.archive",
    fileNameExtension: [".gz", ".gzip"],
    mimeType: ["application/gzip", "application/x-gzip"]
  },
  {
    name: "org.gnu.gnu-zip-tar-archive",
    conformsTo: "org.gnu.gnu-zip-archive",
    mimeType: "application/tar+gzip",
    fileNameExtension: [".tar.gz", ".tgz"]
  },
  {
    name: "com.apple.xar-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".xar"
  },
  {
    name: "public.xz-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".xz",
    mimeType: "application/x-xz"
  },
  {
    name: "public.7z-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".7z",
    mimeType: "application/x-7z-compressed"
  },
  {
    name: "public.rar-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".rar",
    mimeType: ["application/x-rar-compressed", "application/vnd.rar"]
  },
  {
    name: "public.cpio-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".cpio",
    mimeType: "application/x-cpio"
  },
  {
    name: "public.rpm-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".rpm",
    mimeType: "application/x-rpm"
  },
  {
    name: "public.ar-archive",
    conformsTo: "public.archive",
    fileNameExtension: [".a",".lib"]
  },
  {
    name: "public.deb-archive",
    conformsTo: "public.ar-archive",
    fileNameExtension: ".deb",
    mimeType: "application/vnd.debian.binary-package"
  },
  {
    name: "public.arch-linux-archive",
    conformsTo: "public.archive",
    fileNameExtension: ".pkg.tar.xz"
  },
  {
    name: "com.apple.package",
    conformsTo: "public.directory"
  },
  {
    name: "com.apple.application",
    conformsTo: "public.data"
  },
  {
    name: "com.apple.application-bundle",
    conformsTo: "com.apple.application",
    fileNameExtension: [
      ".app",
      ".framework",
      ".kext",
      ".plugin",
      ".docset",
      ".xpc",
      ".qlgenerator",
      ".component",
      ".saver",
      ".mdimport"
    ]
  },
  {
    name: "public.disk-image",
    conformsTo: "public.archive"
  },
  {
    name: "public.image",
    conformsTo: ["public.data", "public.content"]
  },
  {
    name: "public.case-insensitive-text"
  },
  {
    name: "public.filename-extension",
    conformsTo: "public.case-insensitive-text"
  },
  {
    name: "public.mime-type",
    conformsTo: "public.case-insensitive-text"
  },
  {
    name: "public.presentation",
    conformsTo: "public.composite-content"
  },
  {
    name: "com.microsoft.word.doc",
    conformsTo: ["public.data", "public.composite-content"],
    fileNameExtension: ".docx"
  },
  {
    name: "com.microsoft.excel.xls",
    conformsTo: ["public.data", "public.composite-content"],
    fileNameExtension: ".xlsx"
  },
  {
    name: "com.microsoft.powerpoint.ppt",
    conformsTo: ["public.data", "public.presentation"],
    fileNameExtension: ".ppt"
  },
  {
    name: "com.microsoft.word.wordml",
    conformsTo: ["public.data", "public.composite-content"]
  },
  {
    name: "public.image",
    conformsTo: ["public.data", "public.content"]
  },
  {
    name: "public.jpeg",
    conformsTo: "public.image",
    fileNameExtension: [".jpg", ".jpeg"]
  },
  {
    name: "com.adobe.postscript",
    conformsTo: "public.image",
    fileNameExtension: [".ps"],
    mimeType: ["application/postscript"]
  },
  {
    name: "net.daringfireball.markdown",
    conformsTo: "public.text",
    fileNameExtension: [".md", ".markdown"],
    mimeType: ["text/markdown"]
  },
  {
    name: "com.apple.disk-image",
    conformsTo: "public.disk-image",
    fileNameExtension: [".dmg", ".smi", ".img"],
    mimeType: ["application/x-apple-diskimage"]
  },
  {
    name: "public.security.private-key",
    conformsTo: "public.data"
  },
  {
    name: "public.security.certificate",
    conformsTo: "public.data"
  },
  {
    name: "public.security.certificate.trust",
    conformsTo: "public.security.certificate"
  },
  {
    name: "public.security.certificate.request",
    conformsTo: "public.data"
  },
  {
    name: "public.pkcs8",
    conformsTo: "public.data",
    fileNameExtension: [".p8", ".key"],
    mimeType: "application/pkcs8"
  },
  {
    name: "public.pkcs10",
    conformsTo: "public.data",
    fileNameExtension: [".p10", ".csr"],
    mimeType: "application/pkcs10"
  },
  {
    name: "public.pkix-cert",
    conformsTo: "public.security.certificate",
    fileNameExtension: ".cer",
    mimeType: "application/pkix-cert"
  },
  {
    name: "public.pkix-crl",
    conformsTo: "public.data",
    fileNameExtension: ".crl",
    mimeType: "application/pkix-crl"
  },
  {
    name: "public.pkcs7-mime",
    conformsTo: "public.data",
    fileNameExtension: ".p7c",
    mimeType: "application/pkcs7-mime"
  },
  {
    name: "public.x-x509-ca-cert",
    conformsTo: "public.security.certificate",
    fileNameExtension: [".crt", ".der"],
    mimeType: "application/x-x509-ca-cert"
  },
  {
    name: "public.x-x509-user-cert",
    conformsTo: "public.security.certificate",
    fileNameExtension: ".crt",
    mimeType: "application/x-x509-user-cert"
  },
  {
    name: "public.x-pkcs7-crl",
    conformsTo: "public.data",
    fileNameExtension: ".crl",
    mimeType: "application/x-pkcs7-crl"
  },
  {
    name: "public.x-pkcs12",
    conformsTo: "public.data",
    fileNameExtension: ".pem",
    mimeType: "application/x-pem-file"
  },
  {
    name: "public.x-pem-file",
    conformsTo: "public.data",
    fileNameExtension: [".p12", ".pfx"],
    mimeType: "application/x-pkcs12"
  },
  {
    name: "public.x-pkcs7-certificates",
    conformsTo: "public.security.certificate",
    fileNameExtension: [".p7b", ".spc"],
    mimeType: "application/x-pkcs7-certificates"
  },
  {
    name: "public.x-pkcs7-certreqresp",
    conformsTo: "public.data",
    fileNameExtension: ".p7r",
    mimeType: "application/x-pkcs7-certreqresp"
  }
];

/**
 * Object representing a UTI
 * @param {string} name
 * @param {string} conforms
 *
 * @property {string} name
 * @property {string} conforms
 */
class UTI {
  constructor(name, conforms) {
    Object.defineProperties(this, {
      name: {
        value: name
      },
      conforms: {
        value: conforms
      }
    });
  }

  /**
   * Check for conformity
   * @param {UTI} other
   * @return {boolean} true if other conforms to the receiver
   */
  conformsTo(other) {
    if (this === other || this.conforms.has(other)) {
      return true;
    }

    for (const u of this.conforms) {
      if (u.conformsTo(other)) {
        return true;
      }
    }
    return false;
  }

  toString() {
    return this.name;
  }

  /**
   * Deliver JSON representation of the UTI.
   * Sample result
   * ´´´json
   * {
   *   "name": "myUTI",
   *   "conformsTo": [ "uti1", "uti2"]
   * }
   * ´´´
   * @return {Object} json representation of the UTI
   */
  toJSON() {
    return {
      name: this.name,
      conforms: this.conforms
    };
  }
}

/**
 * Registry of UTIs
 * @property {Map<string,UTI>} registry
 * @property {Map<string,UTI>} utiByMimeType
 * @property {Map<string,UTI>} utiByFileNameExtension
 */
export class UTIController {
  constructor() {
    Object.defineProperties(this, {
      registry: {
        value: new Map()
      },
      utiByMimeType: {
        value: new Map()
      },
      utiByFileNameExtension: {
        value: new Map()
      }
    });

    this.register(types);
  }

  /**
   * registers additional types
   * @param {Object[]} types
   */
  register(types) {
    for (const u of types) {
      if (u.fileNameExtension !== undefined) {
        this.assignExtensions(
          u.name,
          Array.isArray(u.fileNameExtension)
            ? u.fileNameExtension
            : [u.fileNameExtension]
        );
      }

      if (u.mimeType !== undefined) {
        this.assignMimeTypes(
          u.name,
          Array.isArray(u.mimeType) ? u.mimeType : [u.mimeType]
        );
      }

      const conforms = new Set();

      if (u.conformsTo !== undefined) {
        const ct = Array.isArray(u.conformsTo) ? u.conformsTo : [u.conformsTo];

        ct.forEach(name => {
          const aUTI = this.getUTI(name);
          if (aUTI === undefined) {
            throw new Error(`Referenced UTI not known: ${name}`);
          } else {
            conforms.add(aUTI);
          }
        });
      }

      const nu = new UTI(u.name, conforms);

      this.registry.set(nu.name, nu);
    }
  }

  /**
   * Lookup a given UTI.
   * @param {string} name UTI
   * @return {string} UTI for the given name or undefined if UTI is not present.
   */
  getUTI(name) {
    return this.registry.get(name);
  }

  /**
   * Lookup a UTIs for a mime type.
   * @param {string} mimeType mime type to get UTIs for
   * @return {string} UTI for the given mime type or undefined if no UTI is registerd for the mime type
   */
  getUTIsForMimeType(mimeType) {
    return this.utiByMimeType.get(mimeType);
  }

  /**
   * Lookup a UTI for a file name.
   * First the file name extension is extracted.
   * Then a lookup in the reistered UTIs for file name extions is executed.
   * @param {string} fileName file to detect UTI for
   * @return {string} UTI for the given fileName or undefined if no UTI is registerd for the file names extension
   */
  getUTIsForFileName(fileName) {
    const m = fileName.match(/(\.[a-zA-Z_0-9]+)$/);
    return m ? this.utiByFileNameExtension.get(m[1]) : undefined;
  }

  /**
   * Check whenever two UTI are conformant.
   * If a conforms to b and b conforms to c then a also conforms to c.
   * @param {string} a first UTI
   * @param {string} b second UTI
   * @return {boolean} true if UTI a conforms to UTI b.
   */
  conformsTo(a, b) {
    const ua = this.registry.get(a);
    return ua === undefined ? false : ua.conformsTo(this.registry.get(b));
  }

  /**
   * Lookup a UTI for a file name and check conformance
   * @param {string} fileName file to detect UTI for
   * @param {string} uti to check conformance egainst
   * @return {boolean} ture if utils for file name are conformant
   */
  fileNameConformsTo(fileName, uti) {
    const utis = this.getUTIsForFileName(fileName);
    if (utis === undefined) {
      return false;
    }
    for (const u of utis) {
      if (this.conformsTo(u, uti)) {
        return true;
      }
    }
    return false;
  }

  assignMimeTypes(name, mimTypes) {
    mimTypes.forEach(type => {
      const u = this.utiByMimeType.get(type);
      if (u === undefined) {
        this.utiByMimeType.set(type, [name]);
      } else {
        u.push(name);
      }
    });
  }

  assignExtensions(name, extensions) {
    extensions.forEach(ext => {
      const e = this.utiByFileNameExtension.get(ext);
      if (e === undefined) {
        this.utiByFileNameExtension.set(ext, [name]);
      } else {
        e.push(name);
      }
    });
  }
}

const defaultController = new UTIController()

defaultController.register([
    {
        name: 'public.svg-image',
        conformsTo: ['public.image', 'public.xml'],
        mimeType: 'application/svg+xml',
        fileNameExtension: '.svg'
    },
    {
        name: 'com.micrososft.typescript',
        conformsTo: ['public.source-code'],
        fileNameExtension: '.ts'
    },
    {
        name: 'org.yaml',
        conformsTo: ['public.text'],
        fileNameExtension: [ '.yml', '.yaml' ],
        mimeType: [ 'application/yaml', 'text/yaml' ]
    }
])

export default defaultController