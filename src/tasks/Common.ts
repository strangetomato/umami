import { UmamiContext } from '../Context.ts'
import { VFile } from 'vfile'
import fnv1a from '@sindresorhus/fnv1a'
import fault from 'fault'
import zlib from 'zlib'
import pify from 'pify'
import _ from 'lodash'

import UTI from '../util/UTI'

export async function loadContent(context: UmamiContext, vfile: VFile): VFile {
    context.log.trace("Loading %s", vfile.path)

    if (! await context.fs.existsPromise(vfile.path)) {
        context.log.warn("Cannot load file %s: file does not exist", vfile.path)
        return vfile;
    }

    vfile.contents = await context.fs.readFilePromise(vfile.path, "utf8")

    if (vfile.contents instanceof Buffer) {
        vfile.contents = vfile.contents.toString('utf-8')
    }

    vfile.data.fnv1a = fnv1a(vfile.contents)

    return vfile;
}


export async function saveContent(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot save a file which has no contents")
    if (!vfile.path)
        throw fault("Cannot save a file which has no path")

    context.log.trace("Writing %s", vfile.path)

    if (!await context.fs.existsPromise(vfile.dirname)) {
        await context.fs.mkdirpPromise(vfile.dirname)
    }
    await context.fs.writeFilePromise(vfile.path, vfile.contents);
    return vfile;
}

export async function brotliCompress(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot compress a file which has no contents")

    vfile.contents = await pify(zlib.brotliCompress)(vfile.contents)

    return vfile
}

export async function brotliCompressTransparent(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot compress a file which has no contents")

    const brfile = _.cloneDeep(vfile)
    brfile.basename += '.br'
    brfile.contents = await pify(zlib.brotliCompress)(vfile.contents)
    await saveContent(context, brfile)
    return vfile;
}

export async function brotliDecompress(context: UmamiContext, vfile: VFile): VFile {
    if (!file.contents)
        throw fault("Cannot decompress a file with no contents")

    vfile.contents = await pify(zlib.brotliDecompress)(vfile.contents)

    return vfile
}

export async function gzipCompress(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot compress a file with no contents")

    vfile.contents = await pify(zlib.gzip)(vfile.contents)

    return vfile
}

export async function gzipCompressTransparent(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault ("Cannot compress a file with no contents")

    const gzfile = _.cloneDeep(vfile)
    gzfile.basename += '.gz'
    gzfile.contents = await pify(zlib.gzip)(vfile.contents);
    await saveContent(context, gzfile);
    return vfile;
}

export async function gzipDecompress(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot decompress a file with no contents")

    vfile.contents = await pify(zlib.gunzip)(vfile.contents)

    return vfile
}