import * as babel from '@babel/core'
import traverse from '@babel/traverse'
import fault from 'fault'
import { VFile } from 'vfile'
import toVFile from 'to-vfile'
import { PnpApi } from '@yarnpkg/pnp'
import { DepGraph } from 'dependency-graph'
import { Flow } from '../util/Flow'
import { ppath } from '@yarnpkg/fslib'

import { loadContent, fnvHash, saveContent, brotliCompressTransparent, gzipCompressTransparent } from './Common'

import { UmamiContext } from '../Context'

import env from '@babel/preset-env'


const babelOpts = babel.loadOptions({
    presets: [ [ env, { "targets": "> 0.25%, not dead", modules: "systemjs" } ] ],
})


export async function parseBabel(context: UmamiContext, vfile: VFile): VFile {
    context.log.trace("Parsing %s", vfile.path)
    if (!vfile.contents)
        throw fault("Cannot parse before content loaded")
    vfile.data.ast = await babel.parseAsync(vfile.contents, babelOpts);
    return vfile;
}

export async function extractImports(context: UmamiContext, vfile: VFile): VFile {
    if (!vfile.data.ast)
        throw fault("Cannot extract imports from syntax tree before parsing")
    vfile.data.imports = []
    traverse(vfile.data.ast, {
        ImportDeclaration: function (path) {
            vfile.data.imports.push(path.node.source.value)
        },
        CallExpression: function (path) {
            if (path.node.callee.type == 'Import' || path.node.callee.type == 'Identifier' && path.node.callee.name == 'require') {
                vfile.data.imports.push(path.node.arguments[0].value)
            }
        }
    })
    return vfile;
}


export async function importTree (context: UmamiContext, vfile: VFile): VFile {

    if (!vfile.data.imports)
        throw fault("Cannot do clever things with a tree of JavaScript imports, when we haven't extracted the list of imports")

    if (!vfile.data.fnv1a)
        throw fault ("FNV1a hash is needed")

    if (!vfile.data.moduleName) {
        vfile.data.moduleName = `${vfile.stem}-${vfile.data.fnv1a}`;
    }

    context.scriptDependencies.addNode(vfile.data.moduleName, vfile);

    for (const dep in vfile.data.imports) {
        const dep_filename = context.pnpAPI.resolveRequest(vfile.data.imports[dep], vfile.path);
        const dep_file = toVFile(dep_filename)
        dep_file.data.moduleName = vfile.data.imports[dep]
        const pipeline = new Flow(context.workQueue)
            .add(loadContent)
            .add(parseBabel)
            .add(extractImports)
            .add(importTree)
            .add(transpile)
            .add((c, v) => { v.path = ppath.join(c.scriptsDir, v.data.moduleName); v.extname = '.js'; return v })
            .add(saveContent)
            .add(brotliCompressTransparent)
            .add(gzipCompressTransparent)

        await pipeline.execute(context, dep_file);

        context.scriptDependencies.addDependency(vfile.data.moduleName, dep_file.data.moduleName)
    }

    return vfile

}

export async function transpile(context: UmamiContext, vfile: VFile): VFile {

    if (!vfile.data.ast)
        throw fault("Cannot transpile with no source AST")

    let { code, map, ast } = await babel.transformFromAstAsync(vfile.data.ast, vfile.contents, babelOpts)

    vfile.contents = code 
    vfile.data.sourceMap = map 
    vfile.data.ast = ast 

    return vfile

}