import _ from 'lodash'
import { ppath } from '@yarnpkg/fslib'

export async function processAssets(ctx: UmamiContext, vfile: VFile): VFile {

    if (!vfile.data.assets) return vfile;

    _.forEach(vfile.data.assets, (asset) => {
        ctx.log.trace("Processing asset %s", asset.src)
        const dst = asset.uri || ppath.join(ctx.assetsDir, _.trimStart(asset.src, '/'));
        ctx.fs.mkdirpSync(ppath.dirname(dst))
        ctx.fs.copyFileSync(asset.src, dst)
        asset.uri = '/' + ppath.relative(ctx.outputDir, dst)
    })

    return vfile

}

