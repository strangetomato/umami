import { UmamiContext } from '../Context'
import { VFile } from 'vfile'
import _ from 'lodash'

export function renderPug(template) {
    return async function (ctx: UmamiContext, vfile: VFile): VFile {
        ctx.log.trace("Rendering pug template for %s", vfile.path)
        const renderContext = {
            ...vfile.data,
            _,
            contents: vfile.contents,
            site: ctx.config.site,
            
            ctx
        }
        vfile.contents = template(renderContext)
        return vfile
    }
}