import YAML from 'yaml'
import fault from 'fault'
import _ from 'lodash'

import { UmamiContext } from '../Context'
import { VFile } from 'vfile'

export async function parseYaml(ctx: UmamiContext, vfile: VFile): VFile {
    if (!vfile.contents)
        throw fault("Cannot parse VFile with no contents")

    _.merge(vfile.data, YAML.parse(vfile.contents))

    if (vfile.data.collection)
        ctx.model.push(vfile)

    return vfile
}

export async function resolveXrefs(ctx: UmamiContext, vfile: VFile): VFile {

    if (!vfile.data.collection) return vfile;

    const xrefs = ctx.getCollectionSpec(vfile.data.collection).xrefs
    if (xrefs) {

        for (const field in xrefs) {
            const xcollectionName = xrefs[field]

            if (vfile.data[field]) {
                vfile.data[field] = _.compact(_.map(vfile.data[field], (v) => {
                    return ctx.getCollection(xcollectionName).find((i) => i.data.slug == v)
                }))

            }
        }

    }

    return vfile;

}