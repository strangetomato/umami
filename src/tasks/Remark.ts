import unified from 'unified'

import remark from 'remark-parse'
import math from 'remark-math'
import slug from 'remark-slug'
import toc from 'remark-toc'
import emoji from 'remark-emoji'
import section from 'remark-sectionize'
import autolinkHeadings from 'remark-autolink-headings'
import externalLinks from 'remark-external-links'
import frontmatter from 'remark-frontmatter'
import extractFrontmatter from 'remark-extract-frontmatter'
import rehype from 'remark-rehype'
import highlight from 'rehype-highlight'
import katex from 'rehype-katex'
import addClasses from 'rehype-add-classes'
import html from 'rehype-stringify'

import slugify from '@sindresorhus/slugify'
import interpolateUrl from 'interpolate-url'
import truncatise from 'truncatise'
import yaml from 'yaml'
import fault from 'fault'
import _ from 'lodash'
import { DateTime } from 'luxon'

import { VFile } from 'vfile'

function createRemarkPipeline(ctx: UmamiContext) {
    return unified()
             .use(remark)
             .use(frontmatter, ['yaml'])
             .use(extractFrontmatter, { yaml: yaml.parse })
             .use(slug)
             .use(autolinkHeadings)
             .use(externalLinks)
             .use(toc)
             .use(math)
             .use(emoji)
             .use(section)
             .use(rehype)
             .use(addClasses, ctx.config.settings.markdown.classes)
             .use(highlight)
             .use(katex)
             .use(html)
}

export async function transformMarkdown(ctx: UmamiContext, vfile: VFile): VFile {
    const remark = createRemarkPipeline(ctx)
    return await remark.process(vfile)
}

export async function postFrontmatter(ctx: UmamiContext, vfile: VFile): VFile {

    if (vfile.data.date) {
        vfile.data.date = DateTime.fromISO(vfile.data.date)
        vfile.data = _.defaults(vfile.data, {
            minute: vfile.data.date.toFormat('mm'),
            hour: vfile.data.date.toFormat('HH'),
            day: vfile.data.date.toFormat('dd'),
            month: vfile.data.date.toFormat('LL'),
            year: vfile.data.date.toFormat('yyyy')
        })
    }

    if (vfile.contents && !vfile.data.excerpt)
        vfile.data.excerpt = truncatise(vfile.contents, { TruncateLength: 150, TruncateBy: "characters", Strict: false})

    if (vfile.data.title && !vfile.data.slug)
        vfile.data.slug = slugify(vfile.data.title)

    if (vfile.data.collection)
        ctx.model.push(vfile)

    return vfile

}

