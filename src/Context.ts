import { ppath, PortablePath, FakeFS, PosixFS, ZipOpenFS } from '@yarnpkg/fslib'
import { getLibzipSync } from '@yarnpkg/libzip'
import { PnpApi } from '@yarnpkg/pnp'
import { findPnpApi } from 'module'
import { DepGraph } from 'dependency-graph'
import { Logger } from '@boost/log'
import { Configuration, CollectionSpec } from './Config'
import { PQueue } from 'p-queue'
import low from 'lowdb'
import lowMemory from 'lowdb/adapters/Memory'
import _ from 'lodash'

export class UmamiContext {

    readonly fs: FakeFS<PortablePath>;
    readonly log: Logger;
    readonly config: Configuration
    readonly pnpAPI: PnpApi;
    readonly workQueue: PQueue;

    readonly scriptDependencies: DepGraph<VFile>;
    readonly model: VFile[]

    constructor(logger: Logger, cfg: Configuration, queue?: PQueue, fs?: FakeFS<PortablePath>) {

        this.config = cfg
        this.log = logger

        this.model = []

        this.workQueue = queue || new PQueue();
        this.fs = fs || new PosixFS(new ZipOpenFS({libzip: getLibzipSync()}));
        this.pnpAPI = findPnpApi(this.config.settings.dirs.base);
        this.scriptDependencies = new DepGraph<VFile>();

    }

    get baseDir(): PortablePath {
        return this.config.settings.dirs.base
    }

    get outputDir(): PortablePath {
        return this.config.settings.dirs.output
    }

    get scriptsDir(): PortablePath {
        return this.config.settings.dirs.scripts
    }

    get stylesDir(): PortablePath {
        return this.config.settings.dirs.styles
    }

    get assetsDir(): PortablePath {
        return this.config.settings.dirs.assets
    }

    getCollection (name: string): VFile[] {
        return _(this.model).filter((v, k) => _.get(v, "data.collection") == name)
    }

    getCollectionSpec (name: string): CollectionSpec {
        return _.find(this.config.collections, (v, k) => v.name == name)
    }



}