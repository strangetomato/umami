import optimal, { object, string, bool, array, shape } from 'optimal'
import { ppath, PortablePath, FakeFS } from '@yarnpkg/fslib'
import { NodeVM, VMScript } from 'vm2'
import fault from 'fault'



const bp_page_spec = {
    uri: string().required().notEmpty(),
    contents: string().notEmpty(),
    template: string().required().notEmpty()
}

const bp_asset_spec = {
    uri: string().required().notEmpty(),
    src: string().required().notEmpty()
}

const bp_view_spec = {
    uri: string().required().notEmpty(),
    src: string().required().notEmpty(),
    canonical: bool()
}

const bp_collection_spec = {
    name: string().required().camelCase().notEmpty(),
    glob: string().required().notEmpty(),
    views: array(shape(bp_view_spec)),
    xrefs: object()
}

const bp_settings_spec = {
    dirs: shape({
        base: string(),
        output: string(),
        scripts: string(),
        styles: string()
    }),
    markdown: shape({
        classes: object()
    })
}

const blueprint = {
    collections: array(shape(bp_collection_spec)),
    pages: array(shape(bp_page_spec)),
    scripts: array(shape(bp_asset_spec)),
    stylesheets: array(shape(bp_asset_spec)),
    assets: array(shape(bp_asset_spec)),
    site: object(),
    settings: shape(bp_settings_spec)
}

export type CollectionSpec = {
    name: string,
    glob: string,
    views: AssetSpec[]
}

export type PageSpec = {
    uri: string,
    contents?: string,
    template: string
}

export type AssetSpec = {
    uri: string,
    src: string
}

export type SettingsSpec = {
    dirs: {
        base: string,
        output: string,
        scripts: string,
        styles: string
    }
}

export type Configuration = {
    collections: CollectionSpec[],
    pages: PageSpec[],
    scripts: AssetSpec[],
    stylesheets: AssetSpec[],
    settings: SettingsSpec,
    site: object
}

export async function findBaseDir (fs: FakeFS<PortablePath>): PortablePath {

    let currentDir = process.cwd()

    while (!await fs.existsPromise(ppath.join(currentDir, 'umami.config.js'))) {
        console.log(`Testing for baseDir: ${currentDir}`)
        if (currentDir == '/') throw fault("Could not determine base directory. No umami.config.js file found.");
        currentDir = ppath.dirname(currentDir)
    }

    return currentDir

}

export async function loadConfiguration (baseDir: PortablePath, fs: FakeFS<PortablePath>): Configuration {

    const existsJs = await fs.existsPromise(ppath.join(baseDir, 'umami.config.js'))

    if (existsJs) {
        const configScriptSrc = await fs.readFilePromise(ppath.join(baseDir, 'umami.config.js'))
        const configScript = new VMScript(configScriptSrc)
        const vm = new NodeVM()

        const loaded = optimal(vm.run(configScript, ppath.join(baseDir, 'umami.config.js')), blueprint)
        loaded.settings.dirs.base ||= baseDir
        loaded.settings.dirs.output ||= ppath.join(loaded.settings.dirs.base, 'dist')
        loaded.settings.dirs.scripts ||= ppath.join(loaded.settings.dirs.output, 'scripts')
        loaded.settings.dirs.styles ||= ppath.join(loaded.settings.dirs.output, 'styles')
        loaded.settings.dirs.assets ||= ppath.join(loaded.settings.dirs.output, 'assets')

        loaded.settings.markdown ||= {}

        return loaded
    }

    return optimal({}, blueprint)

}

