import { parse } from '@boost/args'
import { createLogger } from './util/Log'
import { findBaseDir, loadConfiguration } from './Config'
import { UmamiContext } from './Context'
import { xfs, ppath } from '@yarnpkg/fslib'

import { VFile } from 'vfile'
import toVFile from 'to-vfile'
import vfGlob from 'vfile-glob'
import interpolateUrl from 'interpolate-url'
import pQueue from 'p-queue'


import { loadContent, fnvHash, saveContent, brotliCompressTransparent, gzipCompressTransparent } from './tasks/Common'
import { parseBabel, extractImports, importTree, transpile } from './tasks/Script'
import { transformMarkdown, postFrontmatter } from './tasks/Remark'
import { renderPug } from './tasks/Pug'
import { parseYaml, resolveXrefs } from './tasks/Yaml'
import { processAssets } from './tasks/Assets'

import { Flow } from './util/Flow'
import UTI from './util/UTI'

import os from 'os'
import pug from 'pug'
import _ from 'lodash'

(async () => {

const argv = process.argv.slice(2)

const args = parse<{}>(argv, {
    commands: ['build'],
    options: {
        baseDir: {
            description: 'Base directory of the site to build',
            type: 'string',
            short: 'C',
            default: await findBaseDir(xfs),
            validate (value) { return xfs.existsSync(value) }
        }
    }
})


const log = createLogger()
log.level = 'trace'

log.info("Umami - a savoury static site generator")
log.info("Using base directory: %s", args.options.baseDir)

const config = await loadConfiguration(args.options.baseDir, xfs)

const workQueue = new pQueue({concurrency: os.cpus().length + 1})

const ctx = new UmamiContext(log, config, workQueue)

const collectionEntryFlows = {
    'net.daringfireball.markdown': new Flow(workQueue)
        .add(loadContent)
        .add(transformMarkdown)
        .add(postFrontmatter)
        .add(processAssets),
    'org.yaml': new Flow(workQueue)
        .add(loadContent)
        .add(fnvHash)
        .add(parseYaml)
        .add(processAssets)
    }

for (const i in config.collections) {
    const collection = config.collections[i]

    vfGlob(collection.glob).subscribe(vfile => {

        vfile.data.collection = collection.name;

        const collectionEntryFlow = _.find(collectionEntryFlows, (flow, type) => {
            return UTI.fileNameConformsTo(vfile.path, type)
        })

        if (collectionEntryFlow) collectionEntryFlow.execute(ctx, vfile);

    })

}

await workQueue.onIdle()

const scriptFlow = new Flow(workQueue)
    .add(loadContent)
    .add(parseBabel)
    .add(extractImports)
    .add(importTree)
    .add(transpile)
    .add(async (c, v) => { v.path = ppath.join(c.scriptsDir, v.data.moduleName); v.extname = '.js'; return v; })
    .add(saveContent)
    .add(brotliCompressTransparent)
    .add(gzipCompressTransparent)

for (const i in config.scripts) {

    const vfile = toVFile(ppath.join(ctx.baseDir, config.scripts[i].src))
    vfile.data.moduleName = vfile.stem
    await scriptFlow.execute(ctx, vfile)

}

let importMap = { imports: {}}

const allScripts = ctx.scriptDependencies.overallOrder()

for (const i in allScripts) {
    const script = ctx.scriptDependencies.getNodeData(allScripts[i])

    importMap.imports[script.data.moduleName] = ppath.relative(ctx.outputDir, script.path)

}

await ctx.fs.writeFilePromise(ppath.join(ctx.scriptsDir, 'importmap.json'), JSON.stringify(importMap))

for (const i in config.collections) {
    const collection = config.collections[i]

    for (const j in collection.views) {
        const view = collection.views[j]

        ctx.log.trace("Processing view %s for collection %s", view.src, collection.name)

        const entries = ctx.getCollection(collection.name).value()

        const template = pug.compileFile(ppath.join(ctx.baseDir, view.src))

        const collectionViewFlow = new Flow(workQueue)
            .add(resolveXrefs)
            .add(renderPug(template))
            .add(saveContent)
            .add(brotliCompressTransparent)
            .add(gzipCompressTransparent)

        for (const k in entries) {
            const entry = _.cloneDeep(entries[k])
            entry.data.permalink = interpolateUrl(view.uri, entry.data)

            if (view.canonical) entries[k].data.permalink = entry.data.permalink

            entry.path = ppath.join(ctx.outputDir, _.trimStart(entry.data.permalink, '/'))

            await collectionViewFlow.execute(ctx, entry);
        }

    }

}

for (const i in config.pages) {

    const pageRenderFlow = new Flow(workQueue)

    const page = config.pages[i]

    let vfile = !!page.contents ? toVFile(ppath.join(ctx.baseDir, page.contents)) : new VFile();
    const template = pug.compileFile(ppath.join(ctx.baseDir, page.template))

    vfile.data.permalink = page.uri

    if (!!page.contents) {
        pageRenderFlow
            .add(loadContent)
            .add(transformMarkdown)
        }

    pageRenderFlow
        .add(postFrontmatter)
        .add(processAssets)
        .add(async (c, v) => { v.path = ppath.join(c.outputDir, _.trimStart(v.data.permalink, '/')); return v; })
        .add(renderPug(template))
        .add(saveContent)
        .add(brotliCompressTransparent)
        .add(gzipCompressTransparent)

    await pageRenderFlow.execute(ctx, vfile)

}

await workQueue.onIdle()


})()